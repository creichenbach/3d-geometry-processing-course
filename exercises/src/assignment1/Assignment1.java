package assignment1;

import glWrapper.GLHalfedgeStructure;

import java.io.IOException;
import java.util.Iterator;

import meshes.HalfEdgeStructure;
import meshes.Vertex;
import meshes.WireframeMesh;
import meshes.exception.DanglingTriangleException;
import meshes.exception.MeshNotOrientedException;
import meshes.reader.ObjReader;
import openGL.MyDisplay;

/**
 * 
 * @author Alf
 * 
 */
public class Assignment1 {

	public static void main(String[] args) throws IOException {

		heTest();
		oneNeighborhood();

		valenceAndNormals();
	}

	private static void valenceAndNormals() throws IOException {
		// load a mesh
		WireframeMesh cat = ObjReader.read("./objs/cat.obj", true);
		HalfEdgeStructure heCat = new HalfEdgeStructure();
		try {
			heCat.init(cat);
		} catch (MeshNotOrientedException | DanglingTriangleException e) {
			throw new RuntimeException(e);
		}

		// ..and display it.
		MyDisplay disp = new MyDisplay();

		GLHalfedgeStructure glHeCat = new GLHalfedgeStructure(heCat);

		// choose the shader for the data
		glHeCat.configurePreferredShader("shaders/default.vert",
				"shaders/default.frag", null);

		// add the data to the display
		disp.addToDisplay(glHeCat);

		// do the same but choose a different shader
		GLHalfedgeStructure glHeCat2 = new GLHalfedgeStructure(heCat);
		glHeCat2.configurePreferredShader("shaders/trimesh_flat_val.vert",
				"shaders/trimesh_flat_val.frag",
				"shaders/trimesh_flat_val.geom");
		disp.addToDisplay(glHeCat2);

		// do the same but choose a different shader
		GLHalfedgeStructure glHeCat3 = new GLHalfedgeStructure(heCat);
		glHeCat3.configurePreferredShader("shaders/valence.vert",
				"shaders/valence.frag", null);
		disp.addToDisplay(glHeCat3);
	}

	private static void heTest() throws IOException {
		// load a mesh
		WireframeMesh bunny = ObjReader.read("./objs/bunny5k.obj", true);

		HalfEdgeStructure heBunny = new HalfEdgeStructure();

		try {
			heBunny.init(bunny);
		} catch (MeshNotOrientedException | DanglingTriangleException e) {
			throw new RuntimeException(e);
		}

		// ..and display it.
		MyDisplay disp = new MyDisplay();

		GLHalfedgeStructure glHeBunny = new GLHalfedgeStructure(heBunny);

		// choose the shader for the data
		glHeBunny.configurePreferredShader("shaders/default.vert",
				"shaders/default.frag", null);

		// add the data to the display
		disp.addToDisplay(glHeBunny);

		// do the same but choose a different shader
		GLHalfedgeStructure glHeBunny2 = new GLHalfedgeStructure(heBunny);
		glHeBunny2.configurePreferredShader("shaders/trimesh_flat.vert",
				"shaders/trimesh_flat.frag", "shaders/trimesh_flat.geom");
		disp.addToDisplay(glHeBunny2);
	}

	private static void oneNeighborhood() throws IOException {
		// load a mesh
		WireframeMesh oneNh = ObjReader
				.read("./objs/oneNeighborhood.obj", true);

		HalfEdgeStructure heOneNh = new HalfEdgeStructure();

		try {
			heOneNh.init(oneNh);
		} catch (MeshNotOrientedException | DanglingTriangleException e) {
			throw new RuntimeException(e);
		}

		Vertex center = heOneNh.getVertices().get(0);
		printNeighborsOf(center);
		Vertex one = heOneNh.getVertices().get(1);
		printNeighborsOf(one);
	}

	private static void printNeighborsOf(Vertex vertex) {
		System.out.println("Vertex: " + vertex);
		// Ugh, sysout cannot print iterators. DoodleDebug can do that...
		System.out.println("Neighboring vertices: "
				+ spillIterator(vertex.iteratorVV()));
		System.out.println("Neighboring edges: "
				+ spillIterator(vertex.iteratorVE()));
		System.out.println("Neighboring faces: "
				+ spillIterator(vertex.iteratorVF()));
	}

	private static String spillIterator(Iterator<?> iterator) {
		String result = "[";
		while (iterator.hasNext())
			result += iterator.next() + (iterator.hasNext() ? " " : "");
		return result + "]";
	}

}
