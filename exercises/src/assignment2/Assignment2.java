package assignment2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import glWrapper.GLHashtree;
import glWrapper.GLHashtreeNeighbors;
import glWrapper.GLHashtreeSkeleton;
import glWrapper.GLHashtreeVNeighbors;
import glWrapper.GLHashtree_Vertices;
import glWrapper.GLPointCloud;

import java.io.IOException;

import meshes.PointCloud;
import meshes.reader.ObjReader;
import meshes.reader.PlyReader;
import openGL.MyDisplay;

import org.junit.Test;

public class Assignment2 {

	public static void main(String[] args) throws IOException {

		// these demos will run once all methods in the MortonCodes class are
		// implemented.
		hashTreeDemo(ObjReader.readAsPointCloud("./objs/dragon.obj", true));
		hashTreeDemo(PlyReader.readPointCloud("./objs/octreeTest2.ply", true));

	}

	public static void hashTreeDemo(PointCloud pc) {
		HashOctree tree = new HashOctree(pc, 4, 1, 1f);
		MyDisplay display = new MyDisplay();
		GLPointCloud glPC = new GLPointCloud(pc);
		GLHashtree glOT = new GLHashtree(tree);

		glOT.configurePreferredShader("shaders/octree.vert",
				"shaders/octree.frag", "shaders/octree.geom");

		GLHashtree_Vertices glOTv = new GLHashtree_Vertices(tree);

		GLHashtreeSkeleton glOTSkeleton = new GLHashtreeSkeleton(tree);
		GLHashtreeNeighbors glOTNeighbors = new GLHashtreeNeighbors(tree);
		GLHashtreeVNeighbors glOTVNeighbors = new GLHashtreeVNeighbors(tree);

		display.addToDisplay(glOT);
		display.addToDisplay(glOTv);
		display.addToDisplay(glOTSkeleton);
		display.addToDisplay(glOTNeighbors);
		display.addToDisplay(glOTVNeighbors);

		display.addToDisplay(glPC);

	}

	@SuppressWarnings("unused")
	private static void ownHashTreeDemo(PointCloud pointCloud) {
		GLPointCloud glPointCloud = new GLPointCloud(pointCloud);
		glPointCloud.configurePreferredShader("shaders/octree.vert",
				"shaders/octree.frag", "shaders/octree.geom");

		GLPointCloud glPointCloud2 = new GLPointCloud(pointCloud);
		glPointCloud2.configurePreferredShader("shaders/default.vert",
				"shaders/default.frag", null);

		MyDisplay display = new MyDisplay();
		display.addToDisplay(glPointCloud);
		display.addToDisplay(glPointCloud2);
	}

	@Test
	public void mortonCodesDemo() {

		// example of a level 4 (cell) morton code
		long hash = 0b1000101000100;

		// the hashes of its parent and neighbors
		long parent = 0b1000101000;
		long nbr_plus_x = 0b1000101100000;
		long nbr_plus_y = 0b1000101000110;
		long nbr_plus_z = 0b1000101000101;

		long nbr_minus_x = 0b1000101000000;
		long nbr_minus_y = -1; // invalid: the vertex lies on the boundary and
								// an underflow should occur
		long nbr_minus_z = 0b1000100001101;

		// example of a a vertex morton code in a multigrid of
		// depth 4. It lies on the level 3 and 4 grids
		long vertexHash = 0b1000110100000;

		// you can test your MortonCode methods by checking these results, e.g.
		// as a Junit test
		// Further test at least one case where -z underflow should occur
		// and a case where overflow occurs.

		assertEquals(MortonCodes.parentCode(hash), parent);

		assertTrue(MortonCodes.isCellOnLevelXGrid(hash, 4));

		assertEquals(MortonCodes.nbrCode(hash, 4, 0b100), nbr_plus_x);
		assertEquals(MortonCodes.nbrCode(hash, 4, 0b010), nbr_plus_y);
		assertEquals(MortonCodes.nbrCode(hash, 4, 0b001), nbr_plus_z);

		assertEquals(MortonCodes.nbrCodeMinus(hash, 4, 0b100), nbr_minus_x);
		assertEquals(MortonCodes.nbrCodeMinus(hash, 4, 0b010), nbr_minus_y);
		assertEquals(MortonCodes.nbrCodeMinus(hash, 4, 0b001), nbr_minus_z);

		assertTrue(MortonCodes.isVertexOnLevelXGrid(vertexHash, 3, 4));
		assertTrue(MortonCodes.isVertexOnLevelXGrid(vertexHash, 4, 4));
	}

	@Test
	public void dilatedAddition() {
		long a = 0b010100;
		long b = 0b001110;
		long r = 0b111010;
		assertEquals(MortonCodes.dilAdd(a, b), r);
	}

	@Test
	public void cellLevelTest() {
		long l0 = 0b1;
		long l1 = 0b1010;
		long l2 = 0b1001101;
		long l3 = 0b1011011000;

		assertTrue(MortonCodes.isCellOnLevelXGrid(l0, 0));
		assertTrue(MortonCodes.isCellOnLevelXGrid(l1, 1));
		assertTrue(MortonCodes.isCellOnLevelXGrid(l2, 2));
		assertTrue(MortonCodes.isCellOnLevelXGrid(l3, 3));
	}

	@Test
	public void overflow() {
		long code = 0b1010100;

		assertFalse(MortonCodes.overflowTest(code, 2));
		assertTrue(MortonCodes.overflowTest(code, 1));
	}

	@Test
	public void zSubtractionUnderflow() {
		long code = 0b1110110;

		assertEquals(-1, MortonCodes.nbrCodeMinus(code, 2, 0b001));
	}
}
