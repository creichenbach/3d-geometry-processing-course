package assignment3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.vecmath.Point3f;

import meshes.Point2i;
import meshes.WireframeMesh;
import assignment2.HashOctree;
import assignment2.HashOctreeCell;
import assignment2.HashOctreeVertex;

/**
 * Implwmwnr your Marching cubes algorithms here.
 * 
 * @author bertholet
 * 
 */
public class MarchingCubes {

	// the reconstructed surface
	public WireframeMesh result;

	// the tree to march
	private HashOctree tree;
	// per marchable cube values
	private Map<Integer, Float> val;

	private Map<Point2i, Integer> edgeVertices;

	/**
	 * Implementation of the marching cube algorithm. pass the tree and either
	 * the primary values associated to the trees edges
	 * 
	 * @param tree
	 * @param byLeaf
	 */
	public MarchingCubes(HashOctree tree) {
		this.tree = tree;

		this.edgeVertices = new HashMap<>();
	}

	/**
	 * Perform primary Marching cubes on the tree.
	 */
	public void primaryMC(ArrayList<Float> byVertex) {
		this.val = createVertexMap(byVertex);
		this.result = new WireframeMesh();

		for (HashOctreeCell leaf : tree.getLeafs()) {
			pushCube(leaf);
		}
	}

	private Map<Integer, Float> createVertexMap(ArrayList<Float> byVertex) {
		Map<Integer, Float> result = new HashMap<>();
		for (int i = 0; i < byVertex.size(); i++)
			result.put(i, byVertex.get(i));
		return result;
	}

	/**
	 * Perform dual marchingCubes on the tree
	 */
	public void dualMC(ArrayList<Float> byVertex) {
		this.val = createVertexMap(byVertex); // easier to compute
		this.val = computeCenterValues(byVertex);
		this.result = new WireframeMesh();

		for (HashOctreeVertex vertex : tree.getVertices()) {
			try {
				cubeCornerValues(vertex);
			} catch (NullPointerException e) {
				continue; // was a boundary vertex
			}
			pushCube(vertex);
		}
	}

	private Map<Integer, Float> computeCenterValues(ArrayList<Float> byVertex) {
		Map<Integer, Float> result = new HashMap<>();
		for (HashOctreeCell cell : tree.getCells()) {
			result.put(cell.getIndex(), centerValue(cell));
		}
		return result;
	}

	private Float centerValue(HashOctreeCell cell) {
		float sum = 0;
		int corners = 0;
		for (float cornerValue : cubeCornerValues(cell)) {
			sum += cornerValue;
			corners++;
		}
		assert (corners == 8);
		return sum / corners;
	}

	/**
	 * March a single cube: compute the triangles and add them to the wireframe
	 * model
	 * 
	 * @param n
	 */
	private void pushCube(MarchableCube n) {
		Point2i[] triangle_edges = dummy_edges();
		MCTable.resolve(cubeCornerValues(n), triangle_edges);

		int[] faceIndices = new int[3];
		int index = 0;
		for (Point2i edge : triangle_edges) {
			if (edge.x == -1 & edge.y == -1)
				continue;

			// Multiple triangles per cube
			if (index == 3) {
				if (!faceDegenerated(faceIndices))
					this.result.faces.add(faceIndices);
				faceIndices = new int[3];
				index = 0;
			}

			MarchableCube cornerA = n.getCornerElement(edge.x, tree);
			MarchableCube cornerB = n.getCornerElement(edge.y, tree);
			Point3f a = cornerA.getPosition();
			Point3f b = cornerB.getPosition();
			Point3f interpolated = new Point3f(a);
			float va = this.val.get(cornerA.getIndex());
			float vb = this.val.get(cornerB.getIndex());
			float alpha = va / (va - vb);
			interpolated.interpolate(b, alpha);

			int index1 = Math.min(cornerA.getIndex(), cornerB.getIndex());
			int index2 = Math.max(cornerA.getIndex(), cornerB.getIndex());
			Point2i edgeId = new Point2i(index1, index2);
			int vertexId;
			if (edgeVertices.containsKey(edgeId))
				vertexId = edgeVertices.get(edgeId);
			else {
				vertexId = this.result.vertices.size();
				edgeVertices.put(edgeId, vertexId);
				this.result.vertices.add(interpolated);
			}
			faceIndices[index++] = vertexId;
		}
		if (!faceDegenerated(faceIndices))
			this.result.faces.add(faceIndices);
	}

	private boolean faceDegenerated(int[] faceIndices) {
		assert (faceIndices.length == 3);
		int a = faceIndices[0], b = faceIndices[1], c = faceIndices[2];
		return a == b || b == c || a == c;
	}

	private Point2i[] dummy_edges() {
		return new Point2i[] { new Point2i(-1, -1), new Point2i(-1, -1),
				new Point2i(-1, -1), new Point2i(-1, -1), new Point2i(-1, -1),
				new Point2i(-1, -1), new Point2i(-1, -1), new Point2i(-1, -1),
				new Point2i(-1, -1), new Point2i(-1, -1), new Point2i(-1, -1),
				new Point2i(-1, -1), new Point2i(-1, -1), new Point2i(-1, -1),
				new Point2i(-1, -1) };
	}

	/**
	 * Calculate function values for each cube corner vertex.
	 * 
	 * @param n
	 */
	private float[] cubeCornerValues(MarchableCube n) {
		float[] result = new float[8];
		for (int i = 0; i < 8; i++) {
			result[i] = this.val.get(n.getCornerElement(i, tree).getIndex());
		}
		return result;
	}

	/**
	 * Get a nicely marched wireframe mesh...
	 * 
	 * @return
	 */
	public WireframeMesh getResult() {
		return this.result;
	}

	/**
	 * compute a key from the edge description e, that can be used to uniquely
	 * identify the edge e of the cube n. See Assignment 3 Exerise 1-5
	 * 
	 * @param n
	 * @param e
	 * @return
	 */
	private Point2i key(MarchableCube n, Point2i e) {
		Point2i p = new Point2i(n.getCornerElement(e.x, tree).getIndex(), n
				.getCornerElement(e.y, tree).getIndex());
		if (p.x > p.y) {
			int temp = p.x;
			p.x = p.y;
			p.y = temp;
		}
		return p;
	}

}
