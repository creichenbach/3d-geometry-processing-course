package assignment3;

import java.util.ArrayList;
import java.util.List;

import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;

import meshes.PointCloud;
import sparse.CSRMatrix;
import sparse.CSRMatrix.col_val;
import sparse.LinearSystem;
import assignment2.HashOctree;
import assignment2.HashOctreeCell;
import assignment2.HashOctreeVertex;
import assignment2.MortonCodes;

public class SSDMatrices {

	private static final float lambda0 = 1f;
	private static final float lambda1 = 0.0001f;
	private static final float lambdaR = 0.1f;

	/**
	 * Example Matrix creation: Create an identity matrix, clamped to the
	 * provided format.
	 */
	public static CSRMatrix eye(int nRows, int nCols) {
		CSRMatrix eye = new CSRMatrix(0, nCols);

		// initialize the identiti matrix part
		for (int i = 0; i < Math.min(nRows, nCols); i++) {
			eye.addRow();
			eye.lastRow().add(
			// column i, vlue 1
					new col_val(i, 1));
		}
		// fill up the matrix with empt rows.
		for (int i = Math.min(nRows, nCols); i < nRows; i++) {
			eye.addRow();
		}

		return eye;
	}

	/**
	 * Example matrix creation: Identity matrix restricted to boundary per
	 * vertex values.
	 */
	public static CSRMatrix Eye_octree_boundary(HashOctree tree) {

		CSRMatrix result = new CSRMatrix(0, tree.numberofVertices());

		for (HashOctreeVertex v : tree.getVertices()) {
			if (MortonCodes.isVertexOnBoundary(v.code, tree.getDepth())) {
				result.addRow();
				result.lastRow().add(new col_val(v.index, 1));
			}
		}

		return result;
	}

	/**
	 * One line per point, One column per vertex, enforcing that the
	 * interpolation of the Octree vertex values is zero at the point position.
	 * 
	 */
	public static CSRMatrix D0Term(HashOctree tree, PointCloud cloud) {
		CSRMatrix D0 = new CSRMatrix(cloud.points.size(), tree.getVertices()
				.size());
		for (int i = 0; i < D0.nRows; i++) {
			// D0.rows.get(i).get(cell.getIndex()).val = trilinInterpolate(tree,
			// cloud.points.get(i)); // wrong
			setTrilinInterpolationValues(tree, cloud.points.get(i),
					D0.rows.get(i));

			// for (int j = 0; j < D0.nRows; j++) {
			// D0.rows.get(i).get(j).val = (float) Math.pow(cloud.points.get(i)
			// .distance(tree.getVertices().get(j).getPosition())
			// * fValue, 2);
			// }
		}

		// simple test FIXME
		assertMatch(D0, createVertexPosListX(tree.getVertices()),
				createPointPosListX(cloud.points));
		assert D0.nRows == cloud.points.size();
		assert D0.nCols == tree.getVertices().size();

		return D0;
	}

	private static ArrayList<Float> createVertexPosListX(
			ArrayList<HashOctreeVertex> vertices) {
		ArrayList<Float> result = new ArrayList<>();
		for (HashOctreeVertex vertex : vertices)
			result.add(vertex.getPosition().x);
		return result;
	}

	private static ArrayList<Float> createPointPosListX(
			ArrayList<Point3f> points) {
		ArrayList<Float> result = new ArrayList<>();
		for (Point3f point : points)
			result.add(point.x);
		return result;
	}

	private static void setTrilinInterpolationValues(HashOctree tree,
			Point3f point, ArrayList<col_val> row) {
		HashOctreeCell cell = tree.getCell(point);

		Point3f c000 = cell.getCornerElement(0b000, tree).getPosition();
		Point3f c111 = cell.getCornerElement(0b111, tree).getPosition();

		float xd = (point.x - c000.x) / (c111.x - c000.x);
		float xdI = 1 - xd;
		float yd = (point.y - c000.y) / (c111.y - c000.y);
		float ydI = 1 - yd;
		float zd = (point.z - c000.z) / (c111.z - c000.z);
		float zdI = 1 - zd;

		float w000 = zdI * ydI * xdI;
		float w100 = zdI * ydI * xd;
		float w010 = zdI * yd * xdI;
		float w110 = zdI * yd * xd;
		float w001 = zd * ydI * xdI;
		float w101 = zd * ydI * xd;
		float w011 = zd * yd * xdI;
		float w111 = zd * yd * xd;
		float[] weights = { w000, w001, w010, w011, w100, w101, w110, w111 };

		for (int i = 0b000; i < weights.length; i++) {
			row.add(new col_val(cell.getCornerElement(i, tree).getIndex(),
					weights[i]));
		}
	}

	/**
	 * matrix with three rows per point and 1 column per octree vertex. rows
	 * with i%3 = 0 cover x gradients, =1 y-gradients, =2 z gradients; The row
	 * i, i+1, i+2 correxponds to the point/normal i/3. Three consecutant rows
	 * belong to the same gradient, the gradient in the cell of
	 * pointcloud.point[row/3];
	 */
	public static CSRMatrix D1Term(HashOctree tree, PointCloud cloud) {
		CSRMatrix D1 = new CSRMatrix(3 * cloud.points.size(), tree
				.getVertices().size());
		for (int i = 0; i < D1.nRows; i++) {
			if (i % 3 == 0)
				gradientX(tree, cloud.points.get(i / 3), D1.rows.get(i));
			else if (i % 3 == 1)
				gradientY(tree, cloud.points.get(i / 3), D1.rows.get(i));
			else
				gradientZ(tree, cloud.points.get(i / 3), D1.rows.get(i));
		}

		// FIXME: Wrong by a factor 1.24
		simpleD1Test(tree, D1);
		assert D1.nRows == cloud.points.size() * 3;
		assert D1.nCols == tree.getVertices().size();

		return D1;
	}

	private static void simpleD1Test(HashOctree tree, CSRMatrix D1) {
		ArrayList<Float> func = new ArrayList<>();
		ArrayList<Float> expected = new ArrayList<>();
		float a = 2f, b = 3.5f, c = 4.4f;
		for (HashOctreeVertex vertex : tree.getVertices()) {
			Point3f pos = vertex.getPosition();
			func.add(a * pos.x + b * pos.y + c * pos.z);
		}
		for (int i = 0; i < D1.nRows / 3; i++) {
			expected.add(a);
			expected.add(b);
			expected.add(c);
		}
		assertMatch(D1, func, expected);
	}

	private static void assertMatch(CSRMatrix M, ArrayList<Float> func,
			ArrayList<Float> expected) {
		ArrayList<Float> result = new ArrayList<>();
		M.mult(func, result);
		assert closeEnough(result, expected, 0.1f);
	}

	private static boolean closeEnough(ArrayList<Float> result,
			ArrayList<Float> expected, float error) {
		if (result.size() != expected.size())
			return false;

		for (int i = 0; i < result.size(); i++)
			if (Math.abs(result.get(i) - expected.get(i)) > error)
				return false;

		return true;
	}

	private static void gradientX(HashOctree tree, Point3f point,
			ArrayList<col_val> row) {
		HashOctreeCell cell = tree.getCell(point);
		int[] positives = { 0b100, 0b101, 0b110, 0b111 };
		int[] negatives = { 0b000, 0b001, 0b010, 0b011 };
		setValues(tree, row, cell, positives, 1f / (4 * cell.side));
		setValues(tree, row, cell, negatives, -1f / (4 * cell.side));
	}

	private static void gradientY(HashOctree tree, Point3f point,
			ArrayList<col_val> row) {
		HashOctreeCell cell = tree.getCell(point);
		int[] positives = { 0b010, 0b011, 0b110, 0b111 };
		int[] negatives = { 0b000, 0b001, 0b100, 0b101 };
		setValues(tree, row, cell, positives, 1f / (4 * cell.side));
		setValues(tree, row, cell, negatives, -1f / (4 * cell.side));
	}

	private static void gradientZ(HashOctree tree, Point3f point,
			ArrayList<col_val> row) {
		HashOctreeCell cell = tree.getCell(point);
		int[] positives = { 0b001, 0b011, 0b101, 0b111 };
		int[] negatives = { 0b000, 0b010, 0b100, 0b110 };
		setValues(tree, row, cell, positives, 1f / (4 * cell.side));
		setValues(tree, row, cell, negatives, -1f / (4 * cell.side));
	}

	private static void setValues(HashOctree tree, ArrayList<col_val> row,
			HashOctreeCell cell, int[] cornerIds, float value) {
		for (int cornerId : cornerIds) {
			MarchableCube corner = cell.getCornerElement(cornerId, tree);
			row.add(new col_val(corner.getIndex(), value));
		}
	}

	public static CSRMatrix RTerm(HashOctree tree) {
		CSRMatrix R = new CSRMatrix(0, tree.getVertices().size());
		for (HashOctreeVertex vertex : tree.getVertices()) {
			List<Integer> directions = findTripleDirections(vertex, tree);
			for (Integer direction : directions) {
				R.addRow();
				createNeighborVertexRow(R.lastRow(), tree, vertex, direction);
			}
		}

		// FIXME: Failing
		// simpleRTest(tree, R);
		assert R.nCols == tree.getVertices().size();

		return R;
	}

	private static void simpleRTest(HashOctree tree, CSRMatrix R) {
		ArrayList<Float> func = new ArrayList<>();
		float a = 2f, b = 3.5f, c = 4.4f;
		for (HashOctreeVertex vertex : tree.getVertices()) {
			Point3f pos = vertex.getPosition();
			func.add(a * pos.x + b * pos.y + c * pos.z);
		}

		ArrayList<Float> expected = new ArrayList<>();
		for (@SuppressWarnings("unused")
		ArrayList<col_val> row : R.rows)
			expected.add(0f);

		assertMatch(R, func, expected);
	}

	private static void createNeighborVertexRow(ArrayList<col_val> row,
			HashOctree tree, HashOctreeVertex vertex, int direction) {
		HashOctreeVertex before = tree.getNbr_v2v(vertex, 0b111 ^ direction);
		HashOctreeVertex after = tree.getNbr_v2v(vertex, direction);

		float distBV = before.getPosition().distance(vertex.getPosition());
		float distAV = after.getPosition().distance(vertex.getPosition());
		float distVA = vertex.getPosition().distance(after.getPosition());

		row.add(new col_val(before.getIndex(), 1));
		row.add(new col_val(after.getIndex(), -distBV / (distBV + distAV)));
		row.add(new col_val(vertex.getIndex(), -distVA / (distBV + distAV)));
	}

	private static float calculateTripleDistanceSum(HashOctree tree) {
		float sum = 0;
		for (HashOctreeVertex vertex : tree.getVertices()) {
			List<Integer> directions = findTripleDirections(vertex, tree);
			for (Integer direction : directions) {
				HashOctreeVertex before = tree.getNbr_v2v(vertex,
						0b111 ^ direction);
				HashOctreeVertex after = tree.getNbr_v2v(vertex, direction);

				float distBV = before.getPosition().distance(
						vertex.getPosition());
				float distVA = vertex.getPosition().distance(
						after.getPosition());

				sum += distBV + distVA;
			}
		}
		return sum;
	}

	private static List<Integer> findTripleDirections(HashOctreeVertex vertex,
			HashOctree tree) {
		int[] candidates = { 0b000, 0b001, 0b010, 0b100 };
		List<Integer> result = new ArrayList<>();
		for (int candidate : candidates) {
			HashOctreeVertex neighbor1 = tree.getNbr_v2v(vertex, candidate);
			HashOctreeVertex neighbor2 = tree.getNbr_v2v(vertex,
					0b111 ^ candidate);
			if (neighbor1 != null & neighbor2 != null)
				result.add(candidate);
		}
		return result;
	}

	public static CSRMatrix createFinalMatrix(PointCloud cloud, HashOctree tree) {
		CSRMatrix D0 = SSDMatrices.D0Term(tree, cloud);
		CSRMatrix D1 = SSDMatrices.D1Term(tree, cloud);
		CSRMatrix R = SSDMatrices.RTerm(tree);

		float N = cloud.points.size();
		float sum = calculateTripleDistanceSum(tree);

		CSRMatrix finalMatrix = new CSRMatrix(0, tree.getVertices().size());
		finalMatrix.append(D0,
				(float) Math.sqrt(lambda0) / (float) Math.sqrt(N));
		finalMatrix.append(D1,
				(float) Math.sqrt(lambda1) / (float) Math.sqrt(N));
		finalMatrix.append(R, (float) Math.sqrt(lambdaR) / sum);

		return finalMatrix;
	}

	public static ArrayList<Float> createFinalVector(PointCloud cloud,
			HashOctree tree) {
		CSRMatrix D0 = SSDMatrices.D0Term(tree, cloud);
		CSRMatrix R = SSDMatrices.RTerm(tree);

		float N = cloud.points.size();

		ArrayList<Float> result = new ArrayList<>();
		for (@SuppressWarnings("unused")
		ArrayList<col_val> row : D0.rows)
			result.add(0f);
		for (Vector3f n : cloud.normals) {
			result.add((float) (Math.sqrt(lambda1) / Math.sqrt(N) * n.x));
			result.add((float) (Math.sqrt(lambda1) / Math.sqrt(N) * n.y));
			result.add((float) (Math.sqrt(lambda1) / Math.sqrt(N) * n.z));
		}
		for (@SuppressWarnings("unused")
		ArrayList<col_val> row : R.rows)
			result.add(0f);
		return result;
	}

	/**
	 * Set up the linear system for ssd: append the three matrices,
	 * appropriately scaled. And set up the appropriate right hand side, i.e.
	 * the b in Ax = b
	 * 
	 * @param tree
	 * @param pc
	 * @param lambda0
	 * @param lambda1
	 * @param lambda2
	 * @return
	 */
	public static LinearSystem ssdSystem(HashOctree tree, PointCloud pc,
			float lambda0, float lambda1, float lambda2) {

		LinearSystem system = new LinearSystem();
		system.mat = null;
		system.b = null;
		return system;
	}
}
