package assignment4;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import javax.vecmath.Vector3f;

import meshes.HalfEdgeStructure;
import meshes.Vertex;
import meshes.WireframeMesh;
import meshes.reader.ObjReader;

import org.junit.Before;
import org.junit.Test;

import sparse.CSRMatrix;
import sparse.CSRMatrix.col_val;

public class Assignment4_1_Tests {

	private static final float DELTA = 0.01f;

	// A sphere of radius 2.
	private HalfEdgeStructure hs;
	// An ugly sphere of radius 1, don't expect the Laplacians
	// to perform accurately on this mesh.
	private HalfEdgeStructure hs2;

	@Before
	public void setUp() {
		try {
			WireframeMesh m = ObjReader.read("objs/sphere.obj", false);
			hs = new HalfEdgeStructure();
			hs.init(m);

			m = ObjReader.read("objs/uglySphere.obj", false);
			hs2 = new HalfEdgeStructure();
			hs2.init(m);

		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}

	}

	// Test everything and anything...

	@Test
	public void testRowSums() {
		CSRMatrix uniformLap1 = LMatrices.uniformLaplacian(hs);
		assertRowSumsZero(uniformLap1);
		CSRMatrix uniformLap2 = LMatrices.uniformLaplacian(hs2);
		assertRowSumsZero(uniformLap2);

		CSRMatrix mixedCotLap1 = LMatrices.mixedCotanLaplacian(hs);
		assertRowSumsZero(mixedCotLap1);
		CSRMatrix mixedCotLap2 = LMatrices.mixedCotanLaplacian(hs2);
		assertRowSumsZero(mixedCotLap2);
	}

	private void assertRowSumsZero(CSRMatrix matrix) {
		for (ArrayList<col_val> row : matrix.rows) {
			float sum = 0;
			for (col_val element : row) {
				sum += element.val;
			}
			assertEquals(0, sum, DELTA);
		}
	}

	@Test
	public void testMeanCurvature() {
		CSRMatrix mixedCotLap1 = LMatrices.mixedCotanLaplacian(hs);
		List<Vector3f> normals = computeCurvatureNormals(mixedCotLap1, hs);
		assertCorrectNormals(normals, 1 / 2f, hs);
	}

	private void assertCorrectNormals(List<Vector3f> normals, float length,
			HalfEdgeStructure hs) {
		for (Vertex vertex : hs.getVertices()) {
			assertEquals(length, normals.get(vertex.index).length(), DELTA);
			assertEquals(0,
					vertex.getNormalVector().angle(normals.get(vertex.index)),
					DELTA * Math.PI);
		}
	}

	public static List<Vector3f> computeCurvatureNormals(CSRMatrix laplacian,
			HalfEdgeStructure hs) {
		ArrayList<Vector3f> result = new ArrayList<>();
		LMatrices.mult(laplacian, hs, result);
		for (Vector3f vec : result)
			vec.scale(1 / 2f);
		return result;
	}
}
