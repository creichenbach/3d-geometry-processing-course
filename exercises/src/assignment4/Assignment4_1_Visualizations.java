package assignment4;

import glWrapper.GLHalfedgeStructure;
import glWrapper.GLNormalVectors;

import java.io.IOException;
import java.util.List;

import javax.vecmath.Vector3f;

import openGL.MyDisplay;
import sparse.CSRMatrix;
import meshes.HalfEdgeStructure;
import meshes.WireframeMesh;
import meshes.exception.DanglingTriangleException;
import meshes.exception.MeshNotOrientedException;
import meshes.reader.ObjReader;

public class Assignment4_1_Visualizations {

	public static void main(String[] args) throws IOException,
			MeshNotOrientedException, DanglingTriangleException {
		visualizeSpheres();
		visualizeDragon();
	}

	private static void visualizeDragon() throws IOException,
			MeshNotOrientedException, DanglingTriangleException {
		WireframeMesh m = ObjReader.read("objs/teapot.obj", true);
		HalfEdgeStructure hs = new HalfEdgeStructure();
		hs.init(m);

		displayWithLaplaceNormals(hs, true, true, 0.1f);
	}

	private static void visualizeSpheres() throws IOException,
			MeshNotOrientedException, DanglingTriangleException {
		WireframeMesh m = ObjReader.read("objs/sphere.obj", false);
		HalfEdgeStructure hs = new HalfEdgeStructure();
		hs.init(m);

		m = ObjReader.read("objs/uglySphere.obj", false);
		HalfEdgeStructure hs2 = new HalfEdgeStructure();
		hs2.init(m);

		// display
		displayWithLaplaceNormals(hs, true, true, 0.5f);
		displayWithLaplaceNormals(hs2, false, true, 0.25f);
	}

	public static void displayWithLaplaceNormals(HalfEdgeStructure hs,
			boolean cotangentWeighting, boolean normalize, float scale) {
		MyDisplay disp = new MyDisplay();
		GLHalfedgeStructure glMesh = new GLHalfedgeStructure(hs);
		glMesh.configurePreferredShader("shaders/trimesh_flat.vert",
				"shaders/trimesh_flat.frag", "shaders/trimesh_flat.geom");
		disp.addToDisplay(glMesh);

		// curvature and normals
		CSRMatrix laplacian = LMatrices.uniformLaplacian(hs);
		if (cotangentWeighting)
			laplacian = LMatrices.mixedCotanLaplacian(hs);
		List<Vector3f> normals = Assignment4_1_Tests.computeCurvatureNormals(
				laplacian, hs);

		// display
		GLNormalVectors glNormalVectors = new GLNormalVectors(normals, hs,
				normalize, scale);
		disp.addToDisplay(glNormalVectors);
	}

}
