package assignment4;

import glWrapper.GLHalfedgeStructure;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;

import meshes.Face;
import meshes.HalfEdgeStructure;
import meshes.Vertex;
import meshes.WireframeMesh;
import meshes.exception.DanglingTriangleException;
import meshes.exception.MeshNotOrientedException;
import meshes.reader.ObjReader;
import openGL.MyDisplay;
import sparse.CSRMatrix;
import sparse.CSRMatrix.col_val;
import sparse.solver.JMTSolver;
import sparse.solver.Solver;

/**
 * Smoothing
 * 
 * @author Alf
 * 
 */
public class Assignment4_2_smoothing {

	/**
	 * Equivalent to Mu*dt in lecture
	 */
	private static final float LAMBDA = 0.05f;

	public static void main(String[] args) throws MeshNotOrientedException,
			DanglingTriangleException, IOException {
		// demoWith("objs/uglySphere.obj", true);
		demoWith("objs/cat.obj", false);
	}

	private static void demoWith(String file, boolean cotangentWeighting)
			throws IOException, MeshNotOrientedException,
			DanglingTriangleException {
		WireframeMesh m = ObjReader.read(file, true);
		HalfEdgeStructure hs = new HalfEdgeStructure();
		hs.init(m);

		display(hs);
		smoothImplicit(hs, cotangentWeighting);
		display(hs);
	}

	private static void display(HalfEdgeStructure hs) {
		MyDisplay disp = new MyDisplay();
		GLHalfedgeStructure glMesh = new GLHalfedgeStructure(hs);
		glMesh.configurePreferredShader("shaders/trimesh_flat.vert",
				"shaders/trimesh_flat.frag", "shaders/trimesh_flat.geom");
		disp.addToDisplay(glMesh);
	}

	public static void smoothImplicit(HalfEdgeStructure hs,
			boolean cotangentWeighting) {
		smoothImplicit(hs, cotangentWeighting, LAMBDA);
	}

	public static void smoothImplicit(HalfEdgeStructure hs,
			boolean cotangentWeighting, float lambda) {
		smoothImplicit(hs, cotangentWeighting, lambda, true);
	}

	public static void smoothImplicit(HalfEdgeStructure hs,
			boolean cotangentWeighting, float lambda, boolean rescale) {
		CSRMatrix laplacian = LMatrices.uniformLaplacian(hs);
		if (cotangentWeighting)
			laplacian = LMatrices.mixedCotanLaplacian(hs);

		assert !LMatrices.hasDuplicates(laplacian);

		laplacian.scale(lambda); // XXX minus?
		CSRMatrix matrix = laplacian;
		// add idendity matrix
		for (int i = 0; i < matrix.nRows; i++) {
			boolean hadEntry = false;
			for (col_val entry : matrix.rows.get(i))
				if (entry.col == i) {
					entry.val += 1;
					hadEntry = true;
				}

			if (!hadEntry)
				matrix.rows.get(i).add(new col_val(i, 1));
		}
		// matrix.add(idendity, laplacian);
		// fix for bug in JMTSolver
		for (ArrayList<col_val> row : matrix.rows)
			Collections.sort(row);

		List<Float> px = solveX(hs, matrix);
		List<Float> py = solveY(hs, matrix);
		List<Float> pz = solveZ(hs, matrix);

		float volumeBefore = volume(hs);
		for (Vertex vertex : hs.getVertices()) {
			// XXX: Why minus?
			vertex.getPos().x = px.get(vertex.index);
			vertex.getPos().y = py.get(vertex.index);
			vertex.getPos().z = pz.get(vertex.index);
		}
		float volumeAfter = volume(hs);
		if (rescale)
			scaleMesh(hs, (float) Math.pow(volumeBefore / volumeAfter, 1d / 3));
	}

	private static void scaleMesh(HalfEdgeStructure hs, float factor) {
		for (Vertex vertex : hs.getVertices()) {
			vertex.getPos().scale(factor);
		}
	}

	private static float volume(HalfEdgeStructure hs) {
		float sum = 0;
		for (Face face : hs.getFaces()) {
			Iterator<Vertex> verts = face.iteratorFV();
			Point3f p1 = verts.next().getPos();
			Point3f p2 = verts.next().getPos();
			Point3f p3 = verts.next().getPos();
			Vector3f v1 = new Vector3f(p1);
			Vector3f v2 = new Vector3f(p2);
			Vector3f v3 = new Vector3f(p3);
			Vector3f cross = new Vector3f();
			cross.cross(v2, v3);
			sum += v1.dot(cross);
		}
		return sum / 6;
	}

	private static List<Float> solveX(HalfEdgeStructure hs, CSRMatrix matrix) {
		Solver solver = new JMTSolver();
		ArrayList<Float> px = new ArrayList<>();
		ArrayList<Float> pxAfter = new ArrayList<>();
		for (Vertex vertex : hs.getVertices()) {
			px.add(vertex.getPos().x);
			pxAfter.add(vertex.getPos().x);
		}
		solver.solve(matrix, px, pxAfter);
		return pxAfter;
	}

	private static List<Float> solveY(HalfEdgeStructure hs, CSRMatrix matrix) {
		Solver solver = new JMTSolver();
		ArrayList<Float> p = new ArrayList<>();
		ArrayList<Float> pAfter = new ArrayList<>();
		for (Vertex vertex : hs.getVertices()) {
			p.add(vertex.getPos().y);
			pAfter.add(vertex.getPos().y);
		}
		solver.solve(matrix, p, pAfter);
		return pAfter;
	}

	private static List<Float> solveZ(HalfEdgeStructure hs, CSRMatrix matrix) {
		Solver solver = new JMTSolver();
		ArrayList<Float> p = new ArrayList<>();
		ArrayList<Float> pAfter = new ArrayList<>();
		for (Vertex vertex : hs.getVertices()) {
			p.add(vertex.getPos().z);
			pAfter.add(vertex.getPos().z);
		}
		solver.solve(matrix, p, pAfter);
		return pAfter;
	}

	private static CSRMatrix idendityMatix(int rows) {
		CSRMatrix id = new CSRMatrix(0, rows);
		for (int i = 0; i < rows; i++) {
			id.addRow();
			id.lastRow().add(new col_val(i, 1));
		}
		return id;
	}
}
