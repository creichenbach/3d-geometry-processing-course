package assignment4;

import glWrapper.GLHalfedgeStructure;
import glWrapper.GLWireframeMesh;

import java.io.IOException;

import meshes.HalfEdgeStructure;
import meshes.Vertex;
import meshes.WireframeMesh;
import meshes.exception.DanglingTriangleException;
import meshes.exception.MeshNotOrientedException;
import meshes.reader.ObjReader;
import openGL.MyDisplay;

public class Assignment4_2_uMasking {

	public static void main(String[] arg) throws IOException,
			MeshNotOrientedException, DanglingTriangleException {
		demo("./objs/head.obj");
		// demo("./objs/cat.obj");
		// demo("./objs/dragon.obj");
	}

	private static void demo(String file) throws IOException,
			MeshNotOrientedException, DanglingTriangleException {
		WireframeMesh m = ObjReader.read(file, true);// */

		MyDisplay disp = new MyDisplay();
		GLWireframeMesh glwf = new GLWireframeMesh(m);
		glwf.configurePreferredShader("shaders/trimesh_flat.vert",
				"shaders/trimesh_flat.frag", "shaders/trimesh_flat.geom");
		disp.addToDisplay(glwf);

		HalfEdgeStructure hs = new HalfEdgeStructure();
		hs.init(m);

		// do your unsharp masking thing...
		WireframeMesh mSmooth = ObjReader.read(file, true);
		HalfEdgeStructure hsSmooth = new HalfEdgeStructure();
		hsSmooth.init(mSmooth);
		Assignment4_2_smoothing.smoothImplicit(hsSmooth, true);
		subtract(hs, hsSmooth);

		GLHalfedgeStructure glHs = new GLHalfedgeStructure(hs);
		glHs.configurePreferredShader("shaders/trimesh_flat.vert",
				"shaders/trimesh_flat.frag", "shaders/trimesh_flat.geom");
		disp.addToDisplay(glHs);
	}

	/**
	 * Subtract positions of second from first (store in first).
	 * 
	 * @param hs
	 * @param hs2
	 */
	private static void subtract(HalfEdgeStructure hs, HalfEdgeStructure hs2) {
		assert hs.getVertices().size() == hs2.getVertices().size();
		for (Vertex vertex : hs.getVertices()) {
			Vertex vertex2 = hs2.getVertices().get(vertex.index);

			vertex.getPos().x += vertex.getPos().x - vertex2.getPos().x;
			vertex.getPos().y += vertex.getPos().y - vertex2.getPos().y;
			vertex.getPos().z += vertex.getPos().z - vertex2.getPos().z;
		}
	}

}
