package assignment4;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.ArrayList;

import meshes.HalfEdgeStructure;
import meshes.WireframeMesh;
import meshes.reader.ObjReader;

import org.junit.Before;
import org.junit.Test;

import sparse.CSRMatrix;
import sparse.CSRMatrix.col_val;

public class Assignment4_4_Tests {

	private static final float DELTA = 0.01f;

	// A sphere of radius 2.
	private HalfEdgeStructure hs;
	// An ugly sphere of radius 1, don't expect the Laplacians
	// to perform accurately on this mesh.
	private HalfEdgeStructure hs2;

	private CSRMatrix symmetricCotLap1;

	private CSRMatrix symmetricCotLap2;

	@Before
	public void setUp() {
		try {
			WireframeMesh m = ObjReader.read("objs/sphere.obj", false);
			hs = new HalfEdgeStructure();
			hs.init(m);

			m = ObjReader.read("objs/uglySphere.obj", false);
			hs2 = new HalfEdgeStructure();
			hs2.init(m);

			symmetricCotLap1 = LMatrices.symmetricCotanLaplacian(hs);
			assertRowSumsZero(symmetricCotLap1);
			symmetricCotLap2 = LMatrices.symmetricCotanLaplacian(hs2);
			assertRowSumsZero(symmetricCotLap2);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	public void testRowSums() {
		assertRowSumsZero(symmetricCotLap1);
		assertRowSumsZero(symmetricCotLap2);
	}

	private void assertRowSumsZero(CSRMatrix matrix) {
		for (ArrayList<col_val> row : matrix.rows) {
			float sum = 0;
			for (col_val element : row) {
				sum += element.val;
			}
			assertEquals(0, sum, DELTA);
		}
	}

	@Test
	public void testSymmetry() {
		assertSymmetric(symmetricCotLap1);
		assertSymmetric(symmetricCotLap2);
	}

	private void assertSymmetric(CSRMatrix matrix) {
		for (ArrayList<col_val> row : matrix.rows) {
			int rowIndex = matrix.rows.indexOf(row);
			for (col_val element : row) {
				ArrayList<col_val> mirrorRow = matrix.rows.get(element.col);
				assertEquals(element.val, findElement(mirrorRow, rowIndex).val,
						DELTA);
			}
		}
	}

	private col_val findElement(ArrayList<col_val> mirrorRow, int col) {
		for (col_val element : mirrorRow)
			if (element.col == col)
				return element;
		return null;
	}
}
