package assignment4;

import glWrapper.GLEigenVector;
import glWrapper.GLHalfedgeStructure;

import java.io.IOException;
import java.util.ArrayList;

import javax.vecmath.Vector3f;

import meshes.HalfEdgeStructure;
import meshes.Vertex;
import meshes.WireframeMesh;
import meshes.exception.DanglingTriangleException;
import meshes.exception.MeshNotOrientedException;
import meshes.reader.ObjReader;
import openGL.MyDisplay;
import sparse.CSRMatrix;
import sparse.CSRMatrix.col_val;
import sparse.SCIPYEVD;

/**
 * You can implement the spectral smoothing application here....
 * 
 * @author Alf
 * 
 */
public class Assignment4_4_spectralSmoothing {

	private static final int LOWPASS = 0, MIDPASS = 1, HIGHPASS = 2, NONE = 4;

	public static void main(String[] args) throws IOException,
			MeshNotOrientedException, DanglingTriangleException {
//		 visDemo();

		reconstructionDemo("objs/bunny.obj", 250, LOWPASS);
	}

	private static void reconstructionDemo(String file, int eigenVectors,
			int filterMode) throws IOException, MeshNotOrientedException,
			DanglingTriangleException {
		WireframeMesh mesh = ObjReader.read(file, true);
		HalfEdgeStructure hs = new HalfEdgeStructure();
		hs.init(mesh);

		CSRMatrix symLaplacian = LMatrices.symmetricCotanLaplacian(hs);
		reconstructFromEV(hs, symLaplacian, eigenVectors, filterMode);
	}

	private static void visDemo() throws IOException, MeshNotOrientedException,
			DanglingTriangleException {
		WireframeMesh mesh = ObjReader.read("objs/sphere.obj", true);
		HalfEdgeStructure hs = new HalfEdgeStructure();
		hs.init(mesh);

		CSRMatrix symLaplacian = LMatrices.symmetricCotanLaplacian(hs);

		 visualizeEigenVectors(hs, symLaplacian);
	}

	private static void reconstructFromEV(HalfEdgeStructure hs,
			CSRMatrix symLaplacian, int numVectors, int filterMode)
			throws IOException {
		ArrayList<Float> eigenValues = new ArrayList<>();
		ArrayList<ArrayList<Float>> eigenVectors = new ArrayList<>();
		SCIPYEVD.doSVD(symLaplacian, "first", numVectors, eigenValues,
				eigenVectors);

		CSRMatrix eigenVectorsVert = new CSRMatrix(0, hs.getVertices().size());
		for (ArrayList<Float> eigenVector : eigenVectors) {
			eigenVectorsVert.addRow();
			for (int i = 0; i < eigenVector.size(); i++)
				eigenVectorsVert.lastRow().add(
						new col_val(i, eigenVector.get(i)));
		}
		// TODO: Filter matrix
		CSRMatrix filter = new CSRMatrix(0, numVectors);
		for (ArrayList<Float> eigenVector : eigenVectors) {
			int i = filter.nRows;
			filter.addRow();
			float frequency = (float) Math.sqrt(Math.abs(eigenValues.get(i)));
			float weight = filter(frequency, filterMode);
			filter.lastRow().add(new col_val(i, weight));
		}
		CSRMatrix eigenVectorsHoriz = new CSRMatrix(0, numVectors);
		for (Vertex vertex : hs.getVertices()) {
			int rowNumber = eigenVectorsHoriz.nRows;
			eigenVectorsHoriz.addRow();
			for (int i = 0; i < eigenVectors.size(); i++)
				eigenVectorsHoriz.lastRow().add(
						new col_val(i, eigenVectors.get(i).get(rowNumber)));
		}
		// thanks to assoziativity
		CSRMatrix filteredEvVert = new CSRMatrix(0, 0);
		filter.mult(eigenVectorsVert, filteredEvVert);
		CSRMatrix evMatrix = new CSRMatrix(0, 0);
		eigenVectorsHoriz.mult(filteredEvVert, evMatrix);

		ArrayList<Vector3f> reconstructionPos = new ArrayList<>();
		LMatrices.mult(evMatrix, hs, reconstructionPos);

		for (Vertex vertex : hs.getVertices()) {
			vertex.getPos().x = reconstructionPos.get(vertex.index).x;
			vertex.getPos().y = reconstructionPos.get(vertex.index).y;
			vertex.getPos().z = reconstructionPos.get(vertex.index).z;
		}

		visualize(hs);
	}

	private static float filter(float frequency, int filterMode) {
		switch (filterMode) {
		case LOWPASS:
			return lowpass(frequency);
		case MIDPASS:
			return midpass(frequency);
		case HIGHPASS:
			return highpass(frequency);
		case NONE:
			return 1;
		}
		throw new RuntimeException("Filtermode not recognized");
	}

	private static float highpass(float frequency) {
		return (float) Math.pow(frequency, 2) / 10;
	}

	private static float midpass(float frequency) {
		if (frequency < 2)
			return frequency / 2;
		else if (frequency > 10)
			return 10 / frequency;
		else
			return 1;
	}

	private static float lowpass(float frequency) {
		if (frequency < 4)
			return 1;
		else
			return 4 / frequency;
	}

	private static void visualize(HalfEdgeStructure hs) {
		MyDisplay disp = new MyDisplay();

		GLHalfedgeStructure glHe = new GLHalfedgeStructure(hs);
		glHe.configurePreferredShader("shaders/trimesh_flat.vert",
				"shaders/trimesh_flat.frag", "shaders/trimesh_flat.geom");
		disp.addToDisplay(glHe);
	}

	private static void visualizeEigenVectors(HalfEdgeStructure hs,
			CSRMatrix symLaplacian) throws IOException {
		ArrayList<Float> eigenValues = new ArrayList<>();
		ArrayList<ArrayList<Float>> eigenVectors = new ArrayList<>();
		SCIPYEVD.doSVD(symLaplacian, "first", 20, eigenValues, eigenVectors);

		MyDisplay disp = new MyDisplay();

		// TODO: Loop
		GLEigenVector glEv = new GLEigenVector(hs, eigenVectors.get(12));
		glEv.configurePreferredShader("shaders/trimesh_ev.vert",
				"shaders/trimesh_ev.frag", "shaders/trimesh_ev.geom");
		disp.addToDisplay(glEv);
	}
}
