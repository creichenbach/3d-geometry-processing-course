package assignment4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.vecmath.Vector3f;

import meshes.HalfEdgeStructure;
import meshes.Vertex;
import sparse.CSRMatrix;
import sparse.CSRMatrix.col_val;

/**
 * Methods to create different flavours of the cotangent and uniform laplacian.
 * 
 * @author Alf
 * 
 */
public class LMatrices {

	/**
	 * The uniform Laplacian
	 * 
	 * @param hs
	 * @return
	 */
	public static CSRMatrix uniformLaplacian(HalfEdgeStructure hs) {
		CSRMatrix matrix = new CSRMatrix(0, hs.getVertices().size());
		for (Vertex vertex : hs.getVertices()) {
			matrix.addRow();
			matrix.lastRow().add(new col_val(vertex.index, 1));
			List<Vertex> neighbors = iteratorToList(vertex.iteratorVV());
			int valence = neighbors.size();
			for (Vertex neighbor : neighbors) {
				matrix.lastRow()
						.add(new col_val(neighbor.index, -1f / valence));
			}

			Collections.sort(matrix.lastRow());
		}
		return matrix;
	}

	/**
	 * The cotangent Laplacian
	 * 
	 * @param hs
	 * @return
	 */
	public static CSRMatrix mixedCotanLaplacian(HalfEdgeStructure hs) {
		CSRMatrix matrix = new CSRMatrix(0, hs.getVertices().size());
		for (Vertex vertex : hs.getVertices()) {
			float areaTwice = 2 * vertex.mixedArea();
			matrix.addRow();
			List<Vertex> neighbors = iteratorToList(vertex.iteratorVV());
			float allCotSums = 0;
			for (Vertex neighbor : neighbors) {
				float cotSum = neighbor.cotSum(vertex);
				matrix.lastRow().add(
						new col_val(neighbor.index, -cotSum / areaTwice));
				allCotSums += cotSum;
			}
			matrix.lastRow().add(
					new col_val(vertex.index, allCotSums / areaTwice));

			Collections.sort(matrix.lastRow());
		}
		return matrix;
	}

	/**
	 * Calculate the unnormalized cotangent Laplacian (not devided by any
	 * areas). Border cotan weights are set to zero.
	 * 
	 * @param hs
	 * @return
	 */
	public static CSRMatrix unnormalizedCotanLaplacian(HalfEdgeStructure hs) {
		CSRMatrix matrix = new CSRMatrix(0, hs.getVertices().size());
		for (Vertex vertex : hs.getVertices()) {
			matrix.addRow();

			if (vertex.isOnBorder()) { // XXX: correct?
//				matrix.lastRow().add(new col_val(vertex.index, 1));
				continue;
			}

			List<Vertex> neighbors = iteratorToList(vertex.iteratorVV());
			float allCotSums = 0;
			for (Vertex neighbor : neighbors) {
				float cotSum = neighbor.cotSum(vertex);
				matrix.lastRow().add(new col_val(neighbor.index, -cotSum));
				allCotSums += cotSum;
			}
			matrix.lastRow().add(new col_val(vertex.index, allCotSums));

			Collections.sort(matrix.lastRow());
		}
		return matrix;
	}

	/**
	 * A symmetric cotangent Laplacian, cf Assignment 4, exercise 4.
	 * 
	 * @param hs
	 * @return
	 */
	public static CSRMatrix symmetricCotanLaplacian(HalfEdgeStructure hs) {
		CSRMatrix matrix = new CSRMatrix(0, hs.getVertices().size());
		for (Vertex vertex : hs.getVertices()) {
			matrix.addRow();
			List<Vertex> neighbors = iteratorToList(vertex.iteratorVV());
			float neighborEntries = 0;
			for (Vertex neighbor : neighbors) {
				float cotSum = neighbor.cotSum(vertex);
				float areasRoot = (float) Math.sqrt(vertex.mixedArea()
						* neighbor.mixedArea());
				float neighborEntry = -cotSum / areasRoot;

				// special treatment for borders
				if (vertex.isOnBorder())
					continue;
				if (neighbor.isOnBorder())
					neighborEntry = 0;

				matrix.lastRow()
						.add(new col_val(neighbor.index, neighborEntry));
				neighborEntries += neighborEntry;
			}
			matrix.lastRow().add(new col_val(vertex.index, -neighborEntries));

			Collections.sort(matrix.lastRow());
		}
		return matrix;
	}

	/**
	 * helper method to multiply x,y and z coordinates of the halfedge structure
	 * at once
	 * 
	 * @param m
	 * @param s
	 * @param res
	 */
	public static void mult(CSRMatrix m, HalfEdgeStructure s,
			ArrayList<Vector3f> res) {
		ArrayList<Float> x = new ArrayList<>(), b = new ArrayList<>(s
				.getVertices().size());
		x.ensureCapacity(s.getVertices().size());

		res.clear();
		res.ensureCapacity(s.getVertices().size());
		for (Vertex v : s.getVertices()) {
			x.add(0.f);
			res.add(new Vector3f());
		}

		for (int i = 0; i < 3; i++) {

			// setup x
			for (Vertex v : s.getVertices()) {
				switch (i) {
				case 0:
					x.set(v.index, v.getPos().x);
					break;
				case 1:
					x.set(v.index, v.getPos().y);
					break;
				case 2:
					x.set(v.index, v.getPos().z);
					break;
				}

			}

			m.mult(x, b);

			for (Vertex v : s.getVertices()) {
				switch (i) {
				case 0:
					res.get(v.index).x = b.get(v.index);
					break;
				case 1:
					res.get(v.index).y = b.get(v.index);
					break;
				case 2:
					res.get(v.index).z = b.get(v.index);
					break;
				}

			}
		}
	}

	private static <T> List<T> iteratorToList(Iterator<T> iterator) {
		List<T> list = new ArrayList<>();
		while (iterator.hasNext())
			list.add(iterator.next());
		return list;
	}

	public static boolean hasDuplicates(CSRMatrix mat) {
		for (ArrayList<col_val> r : mat.rows) {
			Collections.sort(r);
			for (int i = 0; i < r.size() - 1; i++) {
				if (r.get(i).col == r.get(i + 1).col) {
					return true;
				}
			}
		}
		return false;
	}
}
