package assignment5;

import meshes.HalfEdgeStructure;
import util.HESUtil;
import util.VisualizationUtil;

public class Assignment5_Epsilon {

	private static final float EPSILON = 0.0001f;

	public static void main(String[] args) {
		HalfEdgeStructure hs = HESUtil.createStructure("buddha.obj");
		VisualizationUtil.visualize(hs);

		HalfEdgeStructure hsAfter = HESUtil.createStructure("buddha.obj");
		int vertsBefore = hsAfter.getVertices().size();
		HalfEdgeCollapse heCollapse = new HalfEdgeCollapse(hsAfter);
		heCollapse.collapseSmallerThan(EPSILON);
		int vertsAfter = hsAfter.getVertices().size();
		System.out.println("Removed " + (vertsBefore - vertsAfter)
				+ " vertices");

		VisualizationUtil.visualize(hsAfter);
	}

}
