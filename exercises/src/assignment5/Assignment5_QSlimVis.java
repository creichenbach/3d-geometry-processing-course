package assignment5;

import meshes.HalfEdgeStructure;
import util.HESUtil;
import util.VisualizationUtil;

public class Assignment5_QSlimVis {
	
	public static void main(String[] args) {
		HalfEdgeStructure hs = HESUtil.createStructure("dragon.obj");
		VisualizationUtil.visualize(hs);
		
		QSlim qSlim = new QSlim(hs);
		qSlim.reduceToVerticeNumber(1500);
		VisualizationUtil.visualize(hs);
	}
}
