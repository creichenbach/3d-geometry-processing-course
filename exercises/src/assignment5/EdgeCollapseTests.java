package assignment5;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import meshes.HalfEdge;
import meshes.HalfEdgeStructure;

import org.junit.Before;
import org.junit.Test;

import util.AssertionUtil;
import util.HESUtil;
import util.VisualizationUtil;

public class EdgeCollapseTests {

	HalfEdgeStructure hsBunny;
	
	public static void main(String[] args) {
		EdgeCollapseTests test = new EdgeCollapseTests();
		test.prepare();
		test.arbitraryRemovals();
	}

	@Before
	public void prepare() {
		hsBunny = HESUtil.createStructure("head.obj");
	}

	@Test
	public void arbitraryRemovals() {
		AssertionUtil.makeSureAssertionsOn();

		HalfEdgeCollapse heCollapse = new HalfEdgeCollapse(hsBunny);

		ArrayList<HalfEdge> edges = hsBunny.getHalfEdges();
		int originalNumVertices = hsBunny.getVertices().size();

		final int aPrime = 3; // 797
		for (int i = 0; i < edges.size(); i += aPrime) {
			// FIXME: Infinite loop when i ~= 11'955...
			// heCollapse.collapseEdgeAndDelete(edges.get(i));
			heCollapse.collapseEdge(edges.get(i));
		}
		int endNumEdges = edges.size();
		heCollapse.finish();

		VisualizationUtil.visualize(hsBunny);
		
		// can be more due to avoided removals (trying to delete already deleted
		// edges)
		assertTrue(originalNumVertices - ((endNumEdges - 1) / aPrime + 1) <= hsBunny
				.getVertices().size());
	}
}
