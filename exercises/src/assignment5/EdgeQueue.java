package assignment5;

import java.util.HashMap;
import java.util.Map;

import meshes.HalfEdge;


public class EdgeQueue {
	
	Map<Float, HalfEdge> prioToEdge;
	Map<HalfEdge, Float> edgeToPrio;
	
	public EdgeQueue() {
		prioToEdge = new HashMap<>();
		edgeToPrio = new HashMap<>();
	}

	public void add(HalfEdge edge, float priority) {
		prioToEdge.put(priority, edge);
		edgeToPrio.put(edge, priority);
	}
	
}
