package assignment5;

import meshes.HalfEdge;

public class PotentialCollapse implements Comparable<PotentialCollapse> {

	private HalfEdge edge;
	private float cost;

	public PotentialCollapse(HalfEdge edge, float cost) {
		this.edge = edge;
		this.cost = 0;
	}

	@Override
	public int compareTo(PotentialCollapse other) {
		return ((Float) this.cost).compareTo(other.cost);
	}

}
