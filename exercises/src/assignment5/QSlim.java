package assignment5;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

import javax.vecmath.Matrix3f;
import javax.vecmath.Matrix4f;
import javax.vecmath.Point3f;
import javax.vecmath.Point4f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;

import meshes.Face;
import meshes.HalfEdge;
import meshes.HalfEdgeStructure;
import meshes.Vertex;
import openGL.objects.Transformation;
import util.CodeUtil;

/**
 * Implement the QSlim algorithm here
 * 
 * @author Alf
 * 
 */
public class QSlim {

	HalfEdgeStructure hs;
	Map<Vertex, Matrix4f> vecErrMatrices;
	PriorityQueue<PotentialCollapse> removalQueue;

	/********************************************
	 * Use or discard the skeletton, as you like.
	 ********************************************/

	public QSlim(HalfEdgeStructure hs) {
		this.hs = hs;
		init();
	}

	/**
	 * Compute per vertex matrices Compute edge collapse costs, Fill up the
	 * Priority queue/heap or similar
	 */
	private void init() {
		vecErrMatrices = new HashMap<>();
		for (Vertex vertex : hs.getVertices()) {
			vecErrMatrices.put(vertex, computeQuadricErrorMatrix(vertex));
		}

		removalQueue = new PriorityQueue<>();
		for (HalfEdge edge : hs.getHalfEdges()) {
			float cost = costOfRemoval(edge);
			removalQueue.add(new PotentialCollapse(edge, cost));
		}
	}

	private float costOfRemoval(HalfEdge edge) {
		Point3f p3 = new Point3f(edge.start().getPos());
		p3.add(edge.end().getPos());
		p3.scale(1 / 2f);
		Point4f p = new Point4f(p3);
		p.w = 1; // XXX

		Matrix4f q = new Matrix4f(vecErrMatrices.get(edge.start()));
		q.add(vecErrMatrices.get(edge.end()));
		return pQp(p, q);
	}

	private float pQp(Point4f p, Matrix4f m) {
		Point4f l = new Point4f(p);
		Point4f r = new Point4f(p);
		m.transform(r);
		return l.x * r.x + l.y * r.y + l.z * r.z + l.w + r.w;
	}

	public void reduceToVerticeNumber(int vertices) {
		int toRemove = hs.getVertices().size() - vertices;
		HalfEdgeCollapse collapse = new HalfEdgeCollapse(hs);
		while (toRemove > 0 && !removalQueue.isEmpty()) {
			System.out.println(removalQueue.size());
			PotentialCollapse candidate = removalQueue.poll();
			System.out.println(removalQueue.size());

			if (candidate.isDeleted || collapse.isEdgeDead(candidate.edge)) {
				candidate.isDeleted = true;
			} else if (!HalfEdgeCollapse.isEdgeCollapsable(candidate.edge)
					|| collapse.isCollapseMeshInv(candidate.edge,
							candidate.edge.end().getPos())) {
				candidate.isDeleted = true;
				System.out.println("Collapsable: "
						+ !HalfEdgeCollapse.isEdgeCollapsable(candidate.edge));
				assert candidate.cost * 0 == 0;
				// if (HalfEdgeCollapse.isEdgeCollapsable(candidate.edge))
				removalQueue.add(new PotentialCollapse(candidate.edge, Math
						.abs(candidate.cost + 0.1f) * 10));
			} else {
				collapse.collapseEdge(candidate.edge, false);
				toRemove--;
			}
			infLoopDetection(toRemove);
		}
	}

	int lastValue = -1;
	int sameValue = 0;

	private void infLoopDetection(int toChange) {
		if (lastValue == toChange)
			sameValue++;
		else {
			lastValue = toChange;
			sameValue = 0;
		}
		if (sameValue >= 10000)
			throw new RuntimeException(
					"Nothing happened for 10000 steps! Probably an inifinite loop...");
	}

	private Matrix4f computeQuadricErrorMatrix(Vertex vertex) {
		Matrix4f result = new Matrix4f();
		for (Face face : CodeUtil.asList(vertex.iteratorVF())) {
			Vector3f n = face.normal();
			Point3f p = vertex.getPos(); // is on plane
			float nDotP = n.dot(new Vector3f(p));

			float[] values = { n.x * n.x, n.x * n.y, n.x * n.z, n.x * -nDotP, //
					n.y * n.x, n.y * n.y, n.y * n.z, n.y * -nDotP, //
					n.z * n.x, n.z * n.y, n.z * n.z, n.z * -nDotP, //
					-nDotP * n.x, -nDotP * n.y, -nDotP * n.z, nDotP * nDotP };
			Matrix4f qFace = new Matrix4f(values);

			if (!containsInfOrNaN(qFace))
				result.add(qFace);
		}
		return result;
	}

	private boolean containsInfOrNaN(Matrix4f m) {
		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 4; j++) {
				float element = m.getElement(i, j);
				if (Float.isInfinite(element) | Float.isNaN(element))
					return true;
			}
		return false;
	}

	/**
	 * The actual QSlim algorithm, collapse edges until the target number of
	 * vertices is reached.
	 * 
	 * @param target
	 */
	public void simplify(int target) {

	}

	/**
	 * Collapse the next cheapest eligible edge. ; this method can be called
	 * until some target number of vertices is reached.
	 */
	public void collapsEdge() {

	}

	/**
	 * helper method that might be useful..
	 * 
	 * @param p
	 * @param ppT
	 */
	private void compute_ppT(Vector4f p, Transformation ppT) {
		assert (p.x * 0 == 0);
		assert (p.y * 0 == 0);
		assert (p.z * 0 == 0);
		assert (p.w * 0 == 0);
		ppT.m00 = p.x * p.x;
		ppT.m01 = p.x * p.y;
		ppT.m02 = p.x * p.z;
		ppT.m03 = p.x * p.w;
		ppT.m10 = p.y * p.x;
		ppT.m11 = p.y * p.y;
		ppT.m12 = p.y * p.z;
		ppT.m13 = p.y * p.w;
		ppT.m20 = p.z * p.x;
		ppT.m21 = p.z * p.y;
		ppT.m22 = p.z * p.z;
		ppT.m23 = p.z * p.w;
		ppT.m30 = p.w * p.x;
		ppT.m31 = p.w * p.y;
		ppT.m32 = p.w * p.z;
		ppT.m33 = p.w * p.w;

	}

	/**
	 * Represent a potential collapse
	 * 
	 * @author Alf
	 * 
	 */
	protected class PotentialCollapse implements Comparable<PotentialCollapse> {

		public boolean isDeleted = false;
		private HalfEdge edge;
		private float cost;

		public PotentialCollapse(HalfEdge edge, float cost) {
			this.edge = edge;
			this.cost = cost;
		}

		@Override
		public int compareTo(PotentialCollapse other) {
			return ((Float) this.cost).compareTo(other.cost);
		}
	}

}
