package assignment5;

import javax.vecmath.Matrix4f;

import meshes.HalfEdgeStructure;
import meshes.Vertex;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import util.HESUtil;

public class QSlimTest {

	HalfEdgeStructure hs;
	QSlim qSlim;

	@Before
	public void prepare() {
		hs = HESUtil.createStructure("buddha.obj");
		qSlim = new QSlim(hs);
	}
	
	@Test
	public void infAndNaN() {
		for (Vertex vertex : hs.getVertices())
		{
			Matrix4f m = qSlim.vecErrMatrices.get(vertex);
			for (int i = 0; i < 4; i++)
				for (int j = 0; j < 4; j++)
				{
					assertFalse(Float.isNaN(m.getElement(i, j)));
					assertFalse(Float.isInfinite(m.getElement(i, j)));
				}
		}
	}
}
