package assignment6;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import javax.rmi.CORBA.Util;
import javax.vecmath.Matrix3f;
import javax.vecmath.Matrix4f;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;

import assignment4.LMatrices;
import meshes.HalfEdgeStructure;
import meshes.Vertex;
import sparse.CSRMatrix;
import sparse.CSRMatrix.col_val;
import sparse.LinearSystem;
import sparse.solver.JMTSolver;
import sparse.solver.Solver;
import util.CodeUtil;
import util.HESUtil;

/**
 * As rigid as possible deformations.
 * 
 * @author Alf
 * 
 */
public class RAPS_modelling {

	/**
	 * ArrayList containing all optimized rotations, keyed by vertex.index
	 */
	ArrayList<Matrix3f> rotations;

	/**
	 * A copy of the original half-edge structure. This is needed to compute the
	 * correct rotation matrices.
	 */
	private HalfEdgeStructure hs_originl;
	/**
	 * The halfedge structure being deformed
	 */
	private HalfEdgeStructure hs_deformed;

	/**
	 * The unnormalized cotan weight matrix, with zero rows for boundary
	 * vertices. It can be computed once at setup time and then be reused to
	 * compute the matrix needed for position optimization
	 */
	CSRMatrix L_cotan;
	/**
	 * The matrix used when solving for optimal positions
	 */
	CSRMatrix L_deform;

	// allocate righthand sides and x only once.
	ArrayList<Float>[] b;
	ArrayList<Float> x;

	// sets of vertex indices that are constrained.
	private HashSet<Integer> keepFixed;
	private HashSet<Integer> deform;

	private static final float CONSTRAINT_WEIGHT = 100;

	/**
	 * The mesh to be deformed
	 * 
	 * @param hs
	 */
	public RAPS_modelling(HalfEdgeStructure hs) {
		this.hs_originl = new HalfEdgeStructure(hs); // deep copy of the
														// original mesh
		this.hs_deformed = hs;

		this.keepFixed = new HashSet<>();
		this.deform = new HashSet<>();

		init_b_x(hs);

	}

	/**
	 * Set which vertices should be kept fixed.
	 * 
	 * @param verts_idx
	 */
	public void keep(Collection<Integer> verts_idx) {
		this.keepFixed.clear();
		this.keepFixed.addAll(verts_idx);

	}

	/**
	 * constrain these vertices to the new target position
	 */
	public void target(Collection<Integer> vert_idx) {
		this.deform.clear();
		this.deform.addAll(vert_idx);
	}

	/**
	 * update the linear system used to find optimal positions for the currently
	 * constrained vertices. Good place to do the cholesky decompositoin
	 */
	public void updateL() {
		// FIXME: Laplacian is probably wrong
		this.L_cotan = LMatrices.unnormalizedCotanLaplacian(this.hs_originl);

		L_deform = new CSRMatrix(0, L_cotan.nCols);
		L_cotan.transposed().mult(L_cotan, L_deform);

		CSRMatrix idConst = createIdConst();
		idConst.scale(CONSTRAINT_WEIGHT * CONSTRAINT_WEIGHT);

		CSRMatrix matCopy = new CSRMatrix(L_deform);
		matCopy.sortRows();
		L_deform.add(matCopy, idConst); // XXX: Might be buggy
		assert L_deform.noDoubleEntries();

		compute_b();
	}

	/**
	 * The RAPS modelling algorithm.
	 * 
	 * @param t
	 * @param nRefinements
	 */
	public void deform(Matrix4f t, int nRefinements) {
		this.transformTarget(t);

		// FIXME: Only single step implemented so far
		while (nRefinements-- > 0)
			optimalPositions();
	}

	private ArrayList<Point3f> calculatePConst() {
		ArrayList<Point3f> result = new ArrayList<>();
		for (Vertex vertex : hs_deformed.getVertices()) {
			Point3f transformed = new Point3f(vertex.getPos());
			result.add(transformed);
		}
		return result;
	}

	private void calcRotations() {
		// for Exercise 1: Simply use idendity
		this.rotations = new ArrayList<>();
		for (Vertex vertex : hs_originl.getVertices()) {
			Matrix3f rot = new Matrix3f();
			rot.setIdentity();
			rotations.add(rot);
		}
	}

	private CSRMatrix createIdConst() {
		ArrayList<Vertex> vertices = hs_originl.getVertices();
		CSRMatrix idConstr = new CSRMatrix(0, vertices.size());
		for (int i = 0; i < vertices.size(); i++) {
			idConstr.addRow();
			if (keepFixed.contains(i) || deform.contains(i))
				idConstr.lastRow().add(new col_val(i, 1f));
		}
		return idConstr;
	}

	/**
	 * Method to transform the target positions and do nothing else.
	 * 
	 * @param t
	 */
	public void transformTarget(Matrix4f t) {
		for (Vertex v : hs_deformed.getVertices()) {
			if (deform.contains(v.index)) {
				t.transform(v.getPos());
			}
		}
	}

	/**
	 * ArrayList keyed with the vertex indices.
	 * 
	 * @return
	 */
	public ArrayList<Matrix3f> getRotations() {
		return rotations;
	}

	/**
	 * Getter for undeformed version of the mesh
	 * 
	 * @return
	 */
	public HalfEdgeStructure getOriginalCopy() {
		return hs_originl;
	}

	/**
	 * initialize b and x
	 * 
	 * @param hs
	 */
	private void init_b_x(HalfEdgeStructure hs) {
		b = new ArrayList[3];
		for (int i = 0; i < 3; i++) {
			b[i] = new ArrayList<>(hs.getVertices().size());
			for (int j = 0; j < hs.getVertices().size(); j++) {
				b[i].add(0.f);
			}
		}
		x = new ArrayList<>(hs.getVertices().size());
		for (int j = 0; j < hs.getVertices().size(); j++) {
			x.add(0.f);
		}
	}

	/**
	 * Compute optimal positions for the current rotations.
	 */
	public void optimalPositions() {
		LinearSystem linSys = new LinearSystem();
		linSys.mat = L_deform;

		CSRMatrix idConst = createIdConst();

		ArrayList<Point3f> pConst = calculatePConst();
		ArrayList<Float> px = CodeUtil.xPart(pConst);
		ArrayList<Float> py = CodeUtil.yPart(pConst);
		ArrayList<Float> pz = CodeUtil.zPart(pConst);
		ArrayList<Float> pxCut = new ArrayList<>();
		ArrayList<Float> pyCut = new ArrayList<>();
		ArrayList<Float> pzCut = new ArrayList<>();
		idConst.mult(px, pxCut);
		idConst.mult(py, pyCut);
		idConst.mult(pz, pzCut);
		// FIXME: px == pxCut here!!!

		CodeUtil.addUp(pxCut, this.b[0]);
		CodeUtil.addUp(pyCut, this.b[1]);
		CodeUtil.addUp(pzCut, this.b[2]);

		Solver solver = new JMTSolver();

		linSys.b = pxCut;
		ArrayList<Float> resultX = new ArrayList<>(px); // initial guess
		solver.solve(linSys, resultX);

		linSys.b = pyCut;
		ArrayList<Float> resultY = new ArrayList<>(py);
		solver.solve(linSys, resultY);

		linSys.b = pzCut;
		ArrayList<Float> resultZ = new ArrayList<>(pz);
		solver.solve(linSys, resultZ);

		for (Vertex vertex : hs_deformed.getVertices()) {
			vertex.getPos().x = pxCut.get(vertex.index);
			vertex.getPos().y = pyCut.get(vertex.index);
			vertex.getPos().z = pzCut.get(vertex.index);
		}
	}

	/**
	 * compute the righthand side for the position optimization
	 */
	private void compute_b() {
		reset_b();

		calcRotations();

		ArrayList<Vector3f> b = new ArrayList<>();
		ArrayList<Vertex> vertices = hs_originl.getVertices();
		for (Vertex vertex : vertices) {
			if (/* keepFixed.contains(vertex.index) && */!vertex.isOnBorder()) { // XXX
				Vector3f sum = new Vector3f();
				for (Vertex neighbor : CodeUtil.asList(vertex.iteratorVV())) {
					Matrix3f rSum = new Matrix3f(rotations.get(neighbor.index));
					rSum.add(rotations.get(vertex.index)); // XXX: Is this
															// correct?

					Vector3f pDiff = new Vector3f(vertex.getPos());
					pDiff.sub(neighbor.getPos());

					rSum.transform(pDiff);
					pDiff.scale(0.5f * neighbor.cotSum(vertex));
					sum.add(pDiff);
				}
				b.add(sum);
			} else {
				b.add(new Vector3f(0, 0, 0));
			}
		}

		ArrayList<Float> bx = CodeUtil.xPart(b);
		ArrayList<Float> by = CodeUtil.yPart(b);
		ArrayList<Float> bz = CodeUtil.zPart(b);
		assert bEqualsLp(bx, L_cotan, HESUtil.xCoordinates(hs_originl));
		assert bEqualsLp(by, L_cotan, HESUtil.yCoordinates(hs_originl));
		assert bEqualsLp(bz, L_cotan, HESUtil.zCoordinates(hs_originl));
		ArrayList<Float> rightHandX = new ArrayList<>();
		ArrayList<Float> rightHandY = new ArrayList<>();
		ArrayList<Float> rightHandZ = new ArrayList<>();
		L_cotan.transposed().mult(bx, rightHandX);
		L_cotan.transposed().mult(by, rightHandY);
		L_cotan.transposed().mult(bz, rightHandZ);

		this.b = new ArrayList[] { rightHandX, rightHandY, rightHandZ }; // XXX
	}

	private boolean bEqualsLp(ArrayList<Float> b, CSRMatrix lap,
			ArrayList<Float> p) {
		ArrayList<Float> result = new ArrayList<>(p.size());
		lap.mult(p, result);

		float delta = 0.01f;
		for (int i = 0; i < result.size(); i++)
			if (Math.abs(result.get(i) - b.get(i)) > delta)
				return false;
		return true;
	}

	/**
	 * helper method
	 */
	private void reset_b() {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < b[i].size(); j++) {
				b[i].set(j, 0.f);
			}
		}
	}

	/**
	 * Compute the optimal rotations for 1-neighborhoods, given the original and
	 * deformed positions.
	 */
	public void optimalRotations() {
		// for the svd.
		Linalg3x3 l = new Linalg3x3(10);// argument controls number of
										// iterations for ed/svd decompositions
										// 3 = very low precision but high
										// speed. 3 seems to be good enough

		// Note: slightly better results are achieved when the absolute of
		// cotangent
		// weights w_ij are used instead of plain cotangent weights.

		// TODO: do your stuff..

	}

	private void compute_ppT(Vector3f p, Vector3f p2, Matrix3f pp2T) {
		assert (p.x * 0 == 0);
		assert (p.y * 0 == 0);
		assert (p.z * 0 == 0);

		pp2T.m00 = p.x * p2.x;
		pp2T.m01 = p.x * p2.y;
		pp2T.m02 = p.x * p2.z;
		pp2T.m10 = p.y * p2.x;
		pp2T.m11 = p.y * p2.y;
		pp2T.m12 = p.y * p2.z;
		pp2T.m20 = p.z * p2.x;
		pp2T.m21 = p.z * p2.y;
		pp2T.m22 = p.z * p2.z;

	}

}
