package assignmentFinal;

import meshes.HalfEdgeStructure;
import util.HESUtil;
import assignmentFinal.UI.MorphDisplay;

public class CoursePeopleMorphs {

	private static String[] names = { "Aaron", "Cedric", "Gian", "Michael",
			"Michelle", "Stefan", "Tiziano", };
	private static String[] files = { "faces/course/aaron_remeshed.obj",
			"faces/course/cedric_remeshed.obj",
			"faces/course/gian_remeshed.obj",
			"faces/course/michael_remeshed.obj",
			"faces/course/michele_remeshed.obj",
			"faces/course/stefan_remeshed.obj",
			"faces/course/tiziano_remeshed.obj" };

	public static void main(String[] args) {
		Morpher morpher = new Morpher(Morpher.AverageMode.MEAN);
		for (int i = 0; i < files.length; i++) {
			String file = files[i];
			String name = names[i];
			System.out.println("Loading " + name + "...");
			HalfEdgeStructure hs = HESUtil.createStructure(file);
			morpher.add(hs, name);
		}

		new MorphDisplay(morpher);
	}
}
