package glWrapper;

import java.util.Collections;
import java.util.List;

import meshes.HalfEdgeStructure;

public class GLEigenVector extends GLHalfedgeStructure {

	public GLEigenVector(HalfEdgeStructure he, List<Float> eigenvector) {
		super(he);

		float max = Collections.max(eigenvector);
		float min = Collections.min(eigenvector);
		float[] ev = new float[eigenvector.size()];
		for (int i = 0; i < ev.length; i++) {
			ev[i] = (eigenvector.get(i) - min) / (max - min);
		}
		this.addElement(ev, Semantic.USERSPECIFIED, 1, "ev");
		// FIXME: Not working
	}

}
