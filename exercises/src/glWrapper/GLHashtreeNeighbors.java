package glWrapper;

import java.util.ArrayList;
import java.util.List;

import javax.media.opengl.GL;
import javax.vecmath.Tuple3f;

import openGL.gl.GLDisplayable;
import openGL.gl.GLRenderer;
import openGL.objects.Transformation;
import assignment2.HashOctree;
import assignment2.HashOctreeCell;

public class GLHashtreeNeighbors extends GLDisplayable {

	public GLHashtreeNeighbors(HashOctree tree) {
		super(getLineEnds(tree).size());

		List<Tuple3f> lineEnds = getLineEnds(tree);

		float[] verts = new float[lineEnds.size() * 3];
		int iv = 0;
		for (Tuple3f lineEnd : lineEnds) {
			verts[iv++] = lineEnd.x;
			verts[iv++] = lineEnd.y;
			verts[iv++] = lineEnd.z;
		}

		int[] ind = new int[lineEnds.size()];
		for (int i = 0; i < ind.length; i++) {
			ind[i] = i;
		}

		this.addElement(verts, Semantic.POSITION, 3);
		this.addIndices(ind);
	}

	/**
	 * Thanks, Java.
	 * 
	 * @param tree
	 * @return
	 */
	private static List<Tuple3f> getLineEnds(HashOctree tree) {
		List<Tuple3f> lineEnds = new ArrayList<>();
		int[] ObxyzArr = { 0b100, 0b010, 0b001 };

		for (HashOctreeCell cell : tree.getLeafs()) {
			for (int Obxyz : ObxyzArr) {
				HashOctreeCell nbr = tree.getNbr_c2c(cell, Obxyz);
				if (nbr == null)
					continue;
				lineEnds.add(cell.center);
				lineEnds.add(nbr.center);
			}
			for (int Obxyz : ObxyzArr) {
				HashOctreeCell nbr = tree.getNbr_c2cMinus(cell, Obxyz);
				if (nbr == null)
					continue;
				lineEnds.add(cell.center);
				lineEnds.add(nbr.center);
			}
		}
		return lineEnds;
	}

	@Override
	public int glRenderFlag() {
		return GL.GL_LINES;
	}

	@Override
	public void loadAdditionalUniforms(GLRenderer glRenderContext,
			Transformation mvMat) {
	}

}
