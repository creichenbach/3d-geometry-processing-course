package glWrapper;

import java.util.ArrayList;
import java.util.List;

import javax.media.opengl.GL;
import javax.vecmath.Tuple3f;

import openGL.gl.GLDisplayable;
import openGL.gl.GLRenderer;
import openGL.objects.Transformation;
import assignment2.HashOctree;
import assignment2.HashOctreeCell;

public class GLHashtreeSkeleton extends GLDisplayable {

	public GLHashtreeSkeleton(HashOctree tree) {
		super(getLineEnds(tree).size());

		List<Tuple3f> lineEnds = getLineEnds(tree);

		float[] verts = new float[lineEnds.size() * 3];
		int iv = 0;
		for (Tuple3f lineEnd : lineEnds) {
			verts[iv++] = lineEnd.x;
			verts[iv++] = lineEnd.y;
			verts[iv++] = lineEnd.z;
		}

		int[] ind = new int[lineEnds.size()];
		for (int i = 0; i < ind.length; i++) {
			ind[i] = i;
		}

		this.addElement(verts, Semantic.POSITION, 3);
		this.addIndices(ind);
	}

	/**
	 * Thanks, Java.
	 * @param tree
	 * @return
	 */
	private static List<Tuple3f> getLineEnds(HashOctree tree) {
		List<Tuple3f> lineEnds = new ArrayList<>();

		for (HashOctreeCell child : tree.getLeafs()) {
			while (child != tree.root) {
				lineEnds.add(child.center);
				lineEnds.add(tree.getParent(child).center);
				child = tree.getParent(child);
			}
		}
		return lineEnds;
	}

	@Override
	public int glRenderFlag() {
		return GL.GL_LINES;
	}

	@Override
	public void loadAdditionalUniforms(GLRenderer glRenderContext,
			Transformation mvMat) {
	}

}
