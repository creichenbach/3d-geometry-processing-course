package glWrapper;

import java.util.List;

import javax.media.opengl.GL;
import javax.vecmath.Vector3f;

import meshes.HalfEdgeStructure;
import meshes.Vertex;
import openGL.gl.GLDisplayable;
import openGL.gl.GLRenderer;
import openGL.objects.Transformation;

public class GLNormalVectors extends GLDisplayable {

	public GLNormalVectors(List<Vector3f> normals, HalfEdgeStructure he,
			boolean normalize, float scale) {
		super(normals.size() * 2);
		assert normals.size() == he.getVertices().size();

		// float[] positionArr = new float[normals.size() * 3];
		// float[] normalArr = new float[normals.size() * 3];
		// for (Vertex vertex : he.getVertices()) {
		// positionArr[3 * vertex.index] = vertex.getPos().x;
		// positionArr[3 * vertex.index + 1] = vertex.getPos().y;
		// positionArr[3 * vertex.index + 2] = vertex.getPos().z;
		// normalArr[3 * vertex.index] = normals.get(vertex.index).x;
		// normalArr[3 * vertex.index + 1] = normals.get(vertex.index).y;
		// normalArr[3 * vertex.index + 2] = normals.get(vertex.index).z;
		// }
		// this.addElement(positionArr, Semantic.POSITION, 3);
		// this.addElement(normalArr, Semantic.USERSPECIFIED, 3, "normal");

		float[] positionArr = new float[normals.size() * 3 * 2];
		for (Vertex vertex : he.getVertices()) {
			Vector3f normal = normals.get(vertex.index);
			normal = new Vector3f(normal);
			if (normalize) {
				normal.normalize();
			}
			normal.scale(scale);

			positionArr[2 * 3 * vertex.index] = vertex.getPos().x;
			positionArr[2 * 3 * vertex.index + 1] = vertex.getPos().y;
			positionArr[2 * 3 * vertex.index + 2] = vertex.getPos().z;
			positionArr[2 * 3 * vertex.index + 3] = vertex.getPos().x
					+ normal.x;
			positionArr[2 * 3 * vertex.index + 4] = vertex.getPos().y
					+ normal.y;
			positionArr[2 * 3 * vertex.index + 5] = vertex.getPos().z
					+ normal.z;
		}
		this.addElement(positionArr, Semantic.POSITION, 3);

		// indices (why?)
		int[] indices = new int[normals.size()*2];
		for (int i = 0; i < indices.length; i++) {
			indices[i] = i;
		}
		this.addIndices(indices);
	}

	@Override
	public int glRenderFlag() {
		return GL.GL_LINES;
	}

	@Override
	public void loadAdditionalUniforms(GLRenderer glRenderContext,
			Transformation mvMat) {
		// no additional uniforms
	}

}
