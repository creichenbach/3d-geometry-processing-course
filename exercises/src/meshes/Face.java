package meshes;

import java.util.Iterator;
import java.util.NoSuchElementException;

import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;

/**
 * Implementation of a face for the {@link HalfEdgeStructure}
 * 
 */
public class Face extends HEElement {

	// an adjacent edge, which is positively oriented with respect to the face.
	private HalfEdge anEdge;

	public Face() {
		anEdge = null;
	}

	public void setHalfEdge(HalfEdge he) {
		this.anEdge = he;
	}

	public HalfEdge getHalfEdge() {
		return anEdge;
	}

	/**
	 * Iterate over the vertices on the face.
	 * 
	 * @return
	 */
	public Iterator<Vertex> iteratorFV() {
		return new IteratorFV(anEdge);
	}

	/**
	 * Iterate over the adjacent edges
	 * 
	 * @return
	 */
	public Iterator<HalfEdge> iteratorFE() {
		return new IteratorFE(anEdge);
	}

	public String toString() {
		if (anEdge == null) {
			return "f: not initialized";
		}
		String s = "f: [";
		Iterator<Vertex> it = this.iteratorFV();
		while (it.hasNext()) {
			s += it.next().toString() + " , ";
		}
		s += "]";
		return s;

	}

	/**
	 * Iterator to iterate over the vertices on a face
	 * 
	 * @author Alf
	 * 
	 */
	public final class IteratorFV implements Iterator<Vertex> {

		private HalfEdge first, actual;

		public IteratorFV(HalfEdge anEdge) {
			first = anEdge;
			actual = null;
		}

		@Override
		public boolean hasNext() {
			return actual == null || actual.next != first;
		}

		@Override
		public Vertex next() {
			// make sure eternam iteration is impossible
			if (!hasNext()) {
				throw new NoSuchElementException();
			}

			// update what edge was returned last
			actual = (actual == null ? first : actual.next);
			return actual.incident_v;
		}

		@Override
		public void remove() {
			// we don't support removing through the iterator.
			throw new UnsupportedOperationException();
		}

		/**
		 * return the face this iterator iterates around
		 * 
		 * @return
		 */
		public Face face() {
			return first.incident_f;
		}
	}

	public final class IteratorFE implements Iterator<HalfEdge> {

		private HalfEdge first, current;

		public IteratorFE(HalfEdge edge) {
			first = edge;
			current = null;
		}

		@Override
		public boolean hasNext() {
			return current == null || current.next != first;
		}

		@Override
		public HalfEdge next() {
			if (!hasNext())
				throw new NoSuchElementException();

			iterationStep();

			return current;
		}

		private void iterationStep() {
			current = current == null ? first : current.next;
		}

		@Override
		public void remove() {
			// me neither
			throw new UnsupportedOperationException();
		}

	}

	public float area() {
		Iterator<HalfEdge> edges = this.iteratorFE();
		Vector3f va = edges.next().vector();
		Vector3f vb = edges.next().vector();
		Vector3f cross = new Vector3f();
		cross.cross(va, vb);
		return cross.length() / 2;
	}

	public float areaByVertices() {
		Iterator<HalfEdge> edges = this.iteratorFE();
		HalfEdge edgeA = edges.next();
		HalfEdge edgeB = edges.next();
		Point3f startA = edgeA.start().getPos();
		Point3f endA = edgeA.end().getPos();
		Vector3f va = new Vector3f(endA.x - startA.x, endA.y - startA.y, endA.z
				- startA.z);
		Point3f startB = edgeB.start().getPos();
		Point3f endB = edgeB.end().getPos();
		Vector3f vb = new Vector3f(endB.x - startB.x, endB.y - startB.y, endB.z
				- startB.z);
		Vector3f cross = new Vector3f();
		cross.cross(va, vb);
		return cross.length() / 2;
	}

	public Vertex obtuseVertex() {
		Iterator<HalfEdge> edges = this.iteratorFE();
		while (edges.hasNext()) {
			HalfEdge edge = edges.next();
			Vector3f va = edge.prev.vector();
			Vector3f vb = edge.vector();
			if (va.angle(vb) < Math.PI / 2)
				return edge.start();
		}
		return null;
	}

	public Vector3f normal() {
		Iterator<HalfEdge> edgeIt = this.iteratorFE();
		Vector3f va = edgeIt.next().vector();
		Vector3f vb = edgeIt.next().vector();
		Vector3f result = new Vector3f();
		result.cross(va, vb);
		result.normalize();
		return result;
	}
}
