package meshes;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;

/**
 * Implementation of a vertex for the {@link HalfEdgeStructure}
 */
public class Vertex extends HEElement {

	/** position */
	Point3f pos;
	/** adjacent edge: this vertex is startVertex of anEdge */
	HalfEdge anEdge;

	/** The index of the vertex, mainly used for toString() */
	public int index;

	public Vertex(Point3f v) {
		pos = v;
		anEdge = null;
	}

	public Point3f getPos() {
		return pos;
	}

	public void setHalfEdge(HalfEdge he) {
		anEdge = he;
	}

	public HalfEdge getHalfEdge() {
		return anEdge;
	}

	/**
	 * Get an iterator which iterates over the 1-neighbouhood
	 * 
	 * @return
	 */
	public Iterator<Vertex> iteratorVV() {
		return new IteratorVV(anEdge);
	}

	/**
	 * Iterate over the incident edges
	 * 
	 * @return
	 */
	public Iterator<HalfEdge> iteratorVE() {
		return new IteratorIncidentVE(anEdge);
	}

	/**
	 * Iterate over the neighboring faces
	 * 
	 * @return
	 */
	public Iterator<Face> iteratorVF() {
		return new IteratorVF(anEdge);
	}

	public String toString() {
		return "" + index;
	}

	public boolean isAdjascent(Vertex w) {
		return this.neighbors().contains(w);

		// boolean isAdj = false;
		// Vertex v = null;
		// Iterator<Vertex> it = iteratorVV();
		// for (v = it.next(); it.hasNext(); v = it.next()) {
		// if (v == w) {
		// isAdj = true;
		// }
		// }
		// return isAdj;
	}

	/**
	 * Iterator over all adjacent halfedges (outgoing and incoming).
	 * 
	 * @author Cedric
	 * 
	 */
	private final class IteratorVE implements Iterator<HalfEdge> {
		private Iterator<HalfEdge> innerIterator;

		// XXX: Infinte loop?
		public IteratorVE(HalfEdge outgoing) {
			List<HalfEdge> edges = new ArrayList<>();
			HalfEdge current = outgoing;
			do {
				edges.add(current);
				current = current.prev;
				edges.add(current);
				current = current.opposite;
			} while (current != outgoing);
			this.innerIterator = edges.iterator();
		}

		@Override
		public boolean hasNext() {
			return innerIterator.hasNext();
		}

		@Override
		public HalfEdge next() {
			return innerIterator.next();
		}

		@Override
		public void remove() {
			innerIterator.remove();
		}
	}

	/**
	 * Iterator over adjacent halfedges (incoming only).
	 * 
	 * @author Cedric
	 * 
	 */
	private final class IteratorIncidentVE implements Iterator<HalfEdge> {
		private Iterator<HalfEdge> innerIterator;

		public IteratorIncidentVE(HalfEdge outgoing) {
			List<HalfEdge> edges = new ArrayList<>();
			HalfEdge current = outgoing;
			do {
				current = current.prev;
				edges.add(current);
				current = current.opposite;
			} while (current != outgoing);
			this.innerIterator = edges.iterator();
		}

		@Override
		public boolean hasNext() {
			return innerIterator.hasNext();
		}

		@Override
		public HalfEdge next() {
			return innerIterator.next();
		}

		@Override
		public void remove() {
			innerIterator.remove();
		}
	}

	/**
	 * Iterator over 1-Neighborhood of this vertex.
	 * 
	 * @author Cedric
	 * 
	 */
	private final class IteratorVV implements Iterator<Vertex> {

		private Iterator<Vertex> innerIterator;

		public IteratorVV(HalfEdge outgoing) {
			Vertex center = outgoing.start();
			Iterator<HalfEdge> edgeIterator = new IteratorVE(center.anEdge);

			List<Vertex> neighbors = new ArrayList<>();
			while (edgeIterator.hasNext()) {
				HalfEdge edge = edgeIterator.next();
				if (edge.start() == center)
					neighbors.add(edge.end());
			}

			innerIterator = neighbors.iterator();
		}

		@Override
		public boolean hasNext() {
			return innerIterator.hasNext();
		}

		@Override
		public Vertex next() {
			return innerIterator.next();
		}

		@Override
		public void remove() {
			innerIterator.remove();
		}

	}

	/**
	 * Iterator over all faces meeting on this vertex.
	 * 
	 * If this vertice has a free side (without any face), there will be a null
	 * as indicator for it.
	 * 
	 * @author Cedric
	 * 
	 */
	private final class IteratorVF implements Iterator<Face> {

		private Iterator<Face> innerIterator;

		public IteratorVF(HalfEdge outgoing) {
			Vertex center = outgoing.start();
			Iterator<HalfEdge> edgeIterator = new IteratorVE(center.anEdge);

			List<Face> neighbors = new ArrayList<>();
			while (edgeIterator.hasNext()) {
				HalfEdge edge = edgeIterator.next();
				if (edge.start() == center)
					neighbors.add(edge.incident_f);
			}

			innerIterator = neighbors.iterator();
		}

		@Override
		public boolean hasNext() {
			return innerIterator.hasNext();
		}

		@Override
		public Face next() {
			return innerIterator.next();
		}

		@Override
		public void remove() {
			innerIterator.remove();
		}

	}

	/**
	 * Calculate a normalized normal vector, calculated from face normal vectors
	 * weighted by incident angle.
	 * 
	 * @return
	 */
	public Vector3f getNormalVector() {
		Iterator<Face> faceIt = this.iteratorVF();
		Vector3f normalVector = new Vector3f(0f, 0f, 0f);
		while (faceIt.hasNext()) {
			Face currentFace = faceIt.next();
			if (currentFace == null)
				continue;

			HalfEdge incoming = null, outgoing = null;
			HalfEdge currentEdge = currentFace.getHalfEdge();
			// iterate over adjacent face edges until those around our vertex
			// are found
			while (incoming == null | outgoing == null) {
				if (currentEdge.start() == this)
					outgoing = currentEdge;
				else if (currentEdge.end() == this)
					incoming = currentEdge;
				currentEdge = currentEdge.next;
			}
			Vector3f vec1 = incoming.vector();
			Vector3f vec2 = outgoing.vector();
			float angle = (float) (Math.PI - vec1.angle(vec2));
			float weight = (float) (2 * Math.PI / angle);

			Vector3f faceNormal = new Vector3f();
			faceNormal.cross(vec1, vec2);
			faceNormal.scale(weight);
			normalVector.add(faceNormal);
		}
		normalVector.normalize();
		return normalVector;
	}

	/**
	 * Computes the mixed voronoi area of this vertex.
	 * 
	 * @return
	 */
	public float mixedArea() {
		float sum = 0;
		Iterator<Face> iterator = this.iteratorVF();
		while (iterator.hasNext()) {
			Face face = iterator.next();
			if (face == null)
				return Float.MAX_VALUE; // weight 0 on borders. XXX: Ok?
			Vertex obtuseVertex = face.obtuseVertex();
			if (obtuseVertex != null)
				if (obtuseVertex.equals(this))
					sum += face.area() / 2;
				else
					sum += face.area() / 4;
			else
				sum += nonObtuseArea(face);
		}
		return sum;
	}

	private float nonObtuseArea(Face face) {
		Iterator<HalfEdge> edges = face.iteratorFE();
		HalfEdge edge = edges.next();
		while (!edge.start().equals(this)) {
			edge = edges.next();
		}

		double line1Square = Math.pow(edge.prev.vector().length(), 2);
		double angle1 = Math.PI - edge.next.vector().angle(edge.vector());
		double line2Square = Math.pow(edge.vector().length(), 2);
		double angle2 = Math.PI - edge.next.vector().angle(edge.prev.vector());

		return (float) ((line1Square * clamp(1 / Math.tan(angle1)) + line2Square
				* clamp(1 / Math.tan(angle2))) / 8);
	}

	/**
	 * Calculate cot(a) + cot(b), where a and b are the third corners of faces
	 * containing this and the parameter vertex.
	 * 
	 * @return
	 */
	public float cotSum(Vertex other) {
		assert this.isNeighbor(other);

		float sum = 0;
		Iterator<Vertex> neighbors = this.iteratorVV();
		while (neighbors.hasNext()) {
			Vertex third = neighbors.next();
			assert third.isNeighbor(this);
			if (other.isNeighbor(third)) {
				Vector3f vec1 = differenceVector(this, third);
				Vector3f vec2 = differenceVector(other, third);
				float angle = vec1.angle(vec2);
				sum += clamp(1 / Math.tan(angle));
			}
		}
		return sum;
	}

	private boolean isNeighbor(Vertex vertex) {
		return this.isAdjascent(vertex) | vertex.isAdjascent(this);
	}

	/**
	 * Clamp to 1e2 (interval [-100,100]) for numerical stability.
	 * 
	 * @param d
	 * @return
	 */
	private float clamp(double d) {
		return (float) Math.max(Math.min(d, 100), -100);
	}

	/**
	 * Create a vector representing A - B.
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	private static Vector3f differenceVector(Vertex a, Vertex b) {
		return new Vector3f(a.pos.x - b.pos.x, a.pos.y - b.pos.y, a.pos.z
				- b.pos.z);
	}

	public List<Vertex> neighbors() {
		Iterator<Vertex> iterator = this.iteratorVV();
		List<Vertex> result = new ArrayList<>();
		while (iterator.hasNext()) {
			result.add(iterator.next());
		}
		return result;
	}

	public boolean isOnBorder() {
		Iterator<Face> faces = this.iteratorVF();
		while (faces.hasNext())
			if (faces.next() == null)
				return true;
		return false;
	}
}
