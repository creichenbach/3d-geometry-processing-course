package util;

public class AssertionUtil {
	/**
	 * Check if assertions are on and break program if not.
	 */
	public static void makeSureAssertionsOn() {
		try {
			assert false;
			throw new RuntimeException("Assertions are off!");
		} catch (AssertionError e) {
			// expected this
		}
	}
}
