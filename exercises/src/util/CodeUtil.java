package util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.vecmath.Tuple3f;

public class CodeUtil {
	public static <T> List<T> asList(Iterator<T> iterator) {
		List<T> result = new ArrayList<>();
		while (iterator.hasNext()) {
			result.add(iterator.next());
		}
		return result;
	}

	public static ArrayList<Float> xPart(ArrayList<? extends Tuple3f> list) {
		ArrayList<Float> result = new ArrayList<>();
		for (Tuple3f element : list)
			result.add(element.x);
		return result;
	}

	public static ArrayList<Float> yPart(ArrayList<? extends Tuple3f> list) {
		ArrayList<Float> result = new ArrayList<>();
		for (Tuple3f element : list)
			result.add(element.y);
		return result;
	}

	public static ArrayList<Float> zPart(ArrayList<? extends Tuple3f> list) {
		ArrayList<Float> result = new ArrayList<>();
		for (Tuple3f element : list)
			result.add(element.z);
		return result;
	}

	/**
	 * For each element of first list, add value of corresponding elements in
	 * second list. They should have the same size.
	 * 
	 * @param base
	 * @param toAdd
	 */
	public static void addUp(ArrayList<Float> base, ArrayList<Float> toAdd) {
		assert base.size() == toAdd.size();

		for (int i = 0; i < base.size(); i++)
			base.set(i, base.get(i) + toAdd.get(i));
	}
}
