package util;

import java.io.IOException;
import java.util.ArrayList;

import meshes.HalfEdgeStructure;
import meshes.Vertex;
import meshes.WireframeMesh;
import meshes.exception.DanglingTriangleException;
import meshes.exception.MeshNotOrientedException;
import meshes.reader.ObjReader;

public class HESUtil {
	public static HalfEdgeStructure createStructure(String filename) {
		try {
			WireframeMesh mesh = ObjReader.read("objs/" + filename, true);
			HalfEdgeStructure hs = new HalfEdgeStructure();
			hs.init(mesh);
			return hs;
		} catch (MeshNotOrientedException | DanglingTriangleException
				| IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static ArrayList<Float> xCoordinates(HalfEdgeStructure hs) {
		ArrayList<Float> result = new ArrayList<>();
		for (Vertex vertex : hs.getVertices())
			result.add(vertex.getPos().x);
		return result;
	}

	public static ArrayList<Float> yCoordinates(HalfEdgeStructure hs) {
		ArrayList<Float> result = new ArrayList<>();
		for (Vertex vertex : hs.getVertices())
			result.add(vertex.getPos().y);
		return result;
	}

	public static ArrayList<Float> zCoordinates(HalfEdgeStructure hs) {
		ArrayList<Float> result = new ArrayList<>();
		for (Vertex vertex : hs.getVertices())
			result.add(vertex.getPos().z);
		return result;
	}
}
