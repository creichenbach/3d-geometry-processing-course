package util;

public class PrintCache {
	private static String cache = "";
	private static int count = 0;
	private static final int SIZE = 1;

	public static void print(String string) {
		cache += "\n" + string;
		if (++count%SIZE == 0) {
			flush();
		}
	}
	
	public static void flush() {
		System.out.print(cache);
		count = 0;
		cache = "";
	}
}
