package util;

import glWrapper.GLHalfedgeStructure;
import openGL.MyDisplay;
import meshes.HalfEdgeStructure;

public class VisualizationUtil {
	public static void visualize(HalfEdgeStructure hs) {
		GLHalfedgeStructure glHs = new GLHalfedgeStructure(hs);
//		glHs.configurePreferredShader("shaders/trimesh_flatColor3f.vert",
//				"shaders/trimesh_flatColor3f.frag",
//				"shaders/trimesh_flatColor3f.geom");
		glHs.configurePreferredShader("shaders/trimesh_flat.vert",
				"shaders/trimesh_flat.frag", "shaders/trimesh_flat.geom");

		MyDisplay display = new MyDisplay();
		display.addToDisplay(glHs);
	}
}
